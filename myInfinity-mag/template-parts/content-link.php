<article class="post post-link">
	<p class="mini-meta">
		<a href="<?php echo get_the_content(); ?>"><?php title(); ?></a>
	</p>
	<?php the_content(); ?>
</article>