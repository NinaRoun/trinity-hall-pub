      <footer class="site-footer">

        <?php if(get_theme_mod('lwp-footer-callout-display') =="Yes") {?>
        <div class="footer-callout clearfix pt-4 pb-4 pl-3 pr-3 mt-4 mb-4">
          <div class="footer-callout-image">
            <img src="<?php echo wp_get_attachment_url(get_theme_mod('lwp-footer-callout-image')); ?>">
          </div>

          <div class="footer-callout-text">

            <h2><a href="<?php echo get_permalink(get_theme_mod('lwp-footer-callout-link')); ?>"><?php echo get_theme_mod('lwp-footer-callout-headline'); ?></a></h2>

            <?php echo wpautop(get_theme_mod('lwp-footer-callout-text')); ?>

          </div>
        </div><!-- /callout mage-->
      <?php } ?>

        <nav class="site-nav">
        	<!-- to take all pages from admin panel -->
        	
        	<?php 
              $args = array('theme_location' => 'footer'); //in header.php it will be "('theme_location' => 'primary')"
        	?>
        	
            <?php wp_nav_menu($args); ?>
        </nav>

    	<p><?php bloginfo('name'); ?> -&copy; <?php echo date('Y'); ?></p>

      </footer>
    </div> <!--  End container class-->

   <?php wp_footer(); ?> <!--  responsible for plugins, widgets  -->
   <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
  </body>
</html>