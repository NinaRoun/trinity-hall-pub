<?php
/**
 * The template for displaying search results pages.
 *
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) : ?>
		
		    <h2>Search results for: <?php the_search_query(); ?></h2>

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				get_template_part('template-parts/content');

			endwhile;

		else :

			echo '<p>No content found</p>';

		endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
