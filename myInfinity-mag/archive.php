<?php  //loop through the posts to output them

get_header();

  if (have_posts()):
  	?>
  	
  		<h2><?php
  			if(is_category()){
  				/*echo 'This is a category';*/
  				single_cat_title();
  			} elseif(is_tag()) {
  				single_tag_title();
  			} elseif(is_author()) {
  				the_post();
  				echo 'Author archives: ' . get_the_author();
  				rewind_posts();
  			} elseif(is_day()) {
  				echo 'Daily archives: ' . get_the_date();
  			} elseif(is_month()) {
  				echo 'Monthly archives: ' . get_the_date('F Y');
  			} elseif(is_year()) {
  				echo 'Yearly archives: ' . get_the_date('Y');
  			} else {
  				echo 'Archive';
  			}
  		?></h2>
  		
  	<?php 
    while(have_posts()): the_post();

        get_template_part('template-parts/content');
        
    endwhile;
  else:
        echo '<p>No content found</p>';

  endif;

  /*get_footer();*/
  ?>
        <footer class="entry-footer">
            <?php
               get_sidebar();
               get_footer(); ?>
        </footer><!-- .entry-footer -->
