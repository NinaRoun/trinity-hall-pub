<?php 
  /*write a function that imports our style*/



  function myOwnTheme_resources(){
  	wp_enqueue_style('style', get_stylesheet_uri());
  }

  add_action('wp_enqueue_scripts', 'myOwnTheme_resources');
  
  //Get top ancestor
  function get_top_ancestor_id(){
    
    global $post;
    
    if($post->post_parent){
      $ancestors = array_reverse(get_post_ancestors($post->ID));    //will create an array of parent id
      return $ancestors[0];
    }
    return $post->ID;
  }
  
  //Does page have children?
  function has_children(){
    
    global $post;
    
   $pages = get_pages('child_of=' . $post->ID); //array
   return count($pages);
    
  }
  
  //Customize excerpt word count length
  function custom_excerpt_length(){
    return 70;
  }
  
  add_filter('excerpt_length', function($length) {
    return 20;
  });
  
  
  //Theme setup
  function ownThemeSetup(){

      register_sidebar(array(
        'name' => 'Sidebar',
        'id' =>'sidebar1',
        'before_widget' => '<div class="widget-item">',
        'after_widget' => '</div>'
      ));
    
      //Navigation menus
      register_nav_menus(array(
        'primary' => __('Primary Menu'),
        'footer' => __('Footer Menu'),
      ));
  
      /*  go to admin panel -> appearance -> menus:
      Menu Name: Primary Menu Links ->create  
      new menu, add categories (go from pages) .

      Create a new (secondary) menu - Footer Menu Links.
      Choose Manage Locations Tabs
      */
    
      //  add Featured Image Support
      add_theme_support('post-thumbnails');
      add_image_size('small-thumbnail', 180, 120, true);/*true-crop the image*/
      add_image_size('banner-image', 920, 510, true);/*920, 810, array('left, top')) - to crop left top*/
      
      //Add post formats support
      add_theme_support('post-formats', array('aside', 'gallery', 'link'));
  }

  add_action('after_setup_theme', 'ownThemeSetup');

  //Add Our Widgets Locations
  // function ourWidgetsInit(){
  //   register_sidebar(array(
  //       'name' => 'Sidebar',
  //       'id' =>'sidebar1',
  //       'before_widget' => '<div class="widget-item">',
  //       'after_widget' => '</div>'
  //   ));
  // }
  //or footer sidebar - https://www.youtube.com/watch?v=QxeQBPgftRE&list=PLpcSpRrAaOaqMA4RdhSnnNcaqOVpX7qi5&index=14

  //add_action('widgets-init', 'ourWidgetsInit');

  //Customize Appearance Objects
  function ownCustomizeRegister($wp_customize){

    $wp_customize->add_setting('lwp_link_color', array(
      'default' => '#8B008B',
      'transport' => 'refresh',
    ));

    $wp_customize->add_setting('lwp_btn_color', array(
      'default' => '#8B008B',
      'transport' => 'refresh',
    ));

    $wp_customize->add_setting('lwp_linkHover_color', array(
      'default' => '#8B008B',
      'transport' => 'refresh',
    ));

    $wp_customize->add_section('lwp_standart_colors', array(
     'title' => __('Standart Colors', 'myInfinity-mag'),
     'priority' =>  30, //where it will sit
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'lwp_link_color_control', array(
        'label' => __('Link Color', 'myInfinity-mag'),
        'section' => 'lwp_standart_colors',
        'settings' => 'lwp_link_color'
    )));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'lwp_btn_color_control', array(
        'label' => __('Button Color', 'myInfinity-mag'),
        'section' => 'lwp_standart_colors',
        'settings' => 'lwp_btn_color'
    )));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'lwp_linkHover_color_control', array(
        'label' => __('Link Hover Color', 'myInfinity-mag'),
        'section' => 'lwp_standart_colors',
        'settings' => 'lwp_linkHover_color'
    )));
  }

  add_action('customize_register', 'ownCustomizeRegister');

  //Output Customize CSS
  function ownCustomizeCSS(){ ?>
    <style type="text/css">
      a:link,
      a:visited{
        color: <?php echo get_theme_mod('lwp_link_color'); ?>;
      }

      .site-header nav ul li.current-menu-item a:link,
      .site-header nav ul li.current-menu-item a:visited
      .site-header nav ul li.current-page-ancestor a:link,
      .site-header nav ul li.current-page-ancestor a:visited{
        background-color: <?php echo get_theme_mod('lwp_link_color'); ?>;
      }

      #searchsubmit{
        background-color: <?php echo get_theme_mod('lwp_btn_color'); ?>;
      }

      .site-header nav ul li a:hover,
      .site-nav ul li a:hover {
        color: <?php echo get_theme_mod('lwp_linkHover_color'); ?>;
      }

    </style>
  <?php };

  add_action('wp_head', 'ownCustomizeCSS');

  //Add Footer callout section to admi appearance customize screen

  function lwp_footer_callout($wp_customize){
    $wp_customize->add_section('lwp-footer-callout-section', array(
      'title' => 'Footer Callout'
    ));

    //asking if admin wants this field to display
    $wp_customize->add_setting('lwp-footer-callout-display', array(
      'default' => 'No'
    ));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'lwp-footer-callout-display-control', array(
      'label' => 'Do you want this section to be displayed?',
      'section' =>'lwp-footer-callout-section',
      'settings' => 'lwp-footer-callout-display',
      'type' => 'select',
      'choices' => array('No' => 'No', 'Yes' => 'Yes')
    )));

    //setting and control for the heading
    $wp_customize->add_setting('lwp-footer-callout-headline', array(
      'default' => 'Example text'
    ));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'lwp-footer-callout-headline-control', array(
      'label' => 'Headline',
      'section' =>'lwp-footer-callout-section',
      'settings' => 'lwp-footer-callout-headline'
    )));

    //setting and control for the paragraph
    $wp_customize->add_setting('lwp-footer-callout-text', array(
      'default' => 'Example paragraph text'
    ));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'lwp-footer-callout-text-control', array(
      'label' => 'Text',
      'section' =>'lwp-footer-callout-section',
      'settings' => 'lwp-footer-callout-text',
      'type' => 'textarea'
    )));

    //setting and control for the link
    $wp_customize->add_setting('lwp-footer-callout-link');

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'lwp-footer-callout-link-control', array(
      'label' => 'Link',
      'section' =>'lwp-footer-callout-section',
      'settings' => 'lwp-footer-callout-link',
      'type' => 'dropdown-pages'
    )));

        //setting and control for the paragraph
    $wp_customize->add_setting('lwp-footer-callout-text', array(
      'default' => 'Example paragraph text'
    ));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'lwp-footer-callout-text-control', array(
      'label' => 'Text',
      'section' =>'lwp-footer-callout-section',
      'settings' => 'lwp-footer-callout-text',
      'type' => 'textarea'
    )));

    //setting and control for the image
    $wp_customize->add_setting('lwp-footer-callout-image');

    $wp_customize->add_control(new WP_Customize_Cropped_Image_Control($wp_customize, 'lwp-footer-callout-image-control', array(
      'label' => 'Image',
      'section' =>'lwp-footer-callout-section',
      'settings' => 'lwp-footer-callout-image',
      'width' => 750,
      'height' => 500
    )));
  }

  add_action('customize_register', 'lwp_footer_callout');

?>