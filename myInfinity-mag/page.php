<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package infinity-mag
 */

get_header(); ?>

			<?php
			if(have_posts()):
				while(have_posts()) : the_post();
			    ?>

	            <article class="post-page">
	            	
	            	<?php 
	            	
	            	  if( has_children() OR $post -> post_parent > 0) { ?>
	            	  
	            	  <nav class="site-nav children-links clearfix">
	            	
	            	    <span class="parent-link"><a href="<?php echo get_the_permalink(get_top_ancestor_id()) ?>"><?php echo get_the_title(get_top_ancestor_id()); ?></a>
	            	    </span>
	            	    
	            	    <ul>
	            
	                        <?php 
	                            $args = array(
	                	            'child_of' => get_top_ancestor_id(),    //creates an array, go to functions.php
	                	            'title_li' => ''
	                	        );
	                        ?>
	            
	                        <?php wp_list_pages($args) ?>
	                    </ul>
	                </nav>
	            	
	            	<?php } ?>
	            	
	            
	                <h2><?php the_title() ?></h2>
	        	    <?php the_content() ?>
	        	</article>
	        
	            <?php endwhile;
	            
	            else:
	            	echo '<p>No content found</p>';
	            	
	        endif;

get_sidebar();
get_footer();
?>