<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
  	<meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <title><?php bloginfo('name'); ?></title>
   <?php wp_head(); ?> <!--  responsible for plugins, widgets  -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
  </head>
  <body <?php body_class(); //add styling?> >

  	<div class="container">

  	<!--  site header -->
  	<header class="site-header">
  	  
  	  <!-- hd-search -->
  	  <div class="hd-search">
  	    <?php get_search_form();  ?>
  	  </div> <!-- /hd-search -->
  	  
  		<h1><a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></h1> <!--  located in settings -> general settings  -->
  		<h5><?php bloginfo('description'); ?>
                <!-- for some extra text to be displayed only in the site-header on the main page, is_page(14) is take from the URL, we could use (is_page('portfolio') as the name of the first tab, and can be given in the URL) -->

      <?php if (is_page(14)){?>
        Thank you for viewing our work!
      <?php }?>

      <!-- --------------------------------------------->
      </h5>

        <nav class="site-nav">
        	<!-- <ul>
        		<li>
        			<a href="#">About us</a>
        			<a href="#">Menus</a>
        			<a href="#">Music</a>
        			<a href="#">Jobs</a>
        		</li>
        	</ul> -->

        	<?php 
              $args = array('theme_location' => 'primary'); //in footer.php it will be "('theme_location' => 'footer')"
        	?>
            <?php wp_nav_menu( $args ); ?>

            <!--  in this way we take pages from admin panel. To take certain pages we'll pass some parametres, just (); (see example in footer.php) - to take all pages.
            We also go to function.php Navigation Menus to register the function -->
        </nav>

  	</header>
  	<!--  /site header -->