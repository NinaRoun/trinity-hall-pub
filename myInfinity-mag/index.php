<?php  //loop through the posts to output them

get_header(); ?>

  <!-- site content -->
  <div class="row site-content clearfix">

    <!-- main column -->
    <div class="main-column col-md-9">
      <?php if (have_posts()):
        while(have_posts()): the_post();

          get_template_part('template-parts/content', get_post_format());
          /*get_template_part('template-parts/content-aside', get_post_format());*/
        
        endwhile;
      else:
        echo '<p>No content found</p>';

      endif; ?>
    </div>
    <!-- /main column -->

    <?php get_sidebar(); ?>

  </div><!-- /site content -->
  
  <?php get_footer();
  ?>

