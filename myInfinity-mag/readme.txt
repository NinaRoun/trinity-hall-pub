=== Infinity Mag ===

Contributors: ThemeInWP

Requires at least: 4.0
Tested up to: 4.9
Stable tag: 1.0.1
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

A clean, elegent and professional Magazine WordPress Theme.

== Description ==
Infinity Mag is a magazine, news or blog WordPress theme with clean and modern design. Infinity Mag is completely responsive and created with most modern technologies. Featuring site-wide or boxed layout, sidebar positions, sticky post, featured posts carousel, custom widgets, and many more useful and interesting features which make your work easier. Infinity Mag WordPress theme can work well on many different kinds of screen, resolution ranging PC, laptops from smart phones. With one click demo import you can setup your dream website from our library of demo sites available to Import . The theme is SEO friendly with optimized code and outstanding support. See our demos: https://demo.themeinwp.com/infinity-mag/ , https://demo.themeinwp.com/infinity-mag/sports-mag/ , https://demo.themeinwp.com/infinity-mag/fashion-mag/ , https://demo.themeinwp.com/infinity-mag/newspaper/ , https://demo.themeinwp.com/infinity-mag/minimal-blog/



== License ==

Infinity Mag WordPress Theme, Copyright (C) 2018, ThemeInWP
Infinity Mag is distributed under the terms of the GNU GPL

== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Credits ==

Underscores:
Author: 2012-2015 Automattic
Source: http://underscores.me
License: GPLv3 or later](https://www.gnu.org/licenses/gpl-3.0.html)

normalize:
Author: 2012-2015 Nicolas Gallagher and Jonathan Neal
Source: http://necolas.github.io/normalize.css
License: [MIT/GPL2 Licensed](http://opensource.org/licenses/MIT)

html5shiv:
Author: 2014 Alexander Farkas (aFarkas)
Source: https://github.com/afarkas/html5shiv
License: [MIT/GPL2 Licensed](http://opensource.org/licenses/MIT)

jquery.easing:
Author: 2008 George McGinley Smith
Source: https://raw.github.com/gdsmith/jquery-easing/master
License: [MIT](http://opensource.org/licenses/MIT)

Bootstrap:
Author: Twitter
Source: http://getbootstrap.com
License: Licensed under the MIT license

BreadcrumbTrail:
Author: Justin Tadlock
Source: http://themehybrid.com/plugins/breadcrumb-trail
License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

Font Awesome:
Author: Dave Gandy
Source: http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
License: Licensed under SIL Open Font License (OFL) and MIT License

slick carousel:
Author: Ken Wheeler
Source: https://github.com/kenwheeler/slick
License: Licensed under the MIT license

theia-sticky-sidebar:
Author: Liviu Cristian Mirea Ghiban
Source: https://github.com/WeCodePixels/theia-sticky-sidebar/blob/master/LICENSE.txt
License: Licensed under the MIT license

sidr:
Author: Alberto Varela
Source: http://www.berriart.com/sidr/
License: Licensed under the MIT license


Magnific Popup:
Author: Dmitry Semenov
Source: https://github.com/dimsemenov/Magnific-Popup
License: Licensed under the MIT license


== Image Used ==
--------------------
Above Slider
--------------------
Author: Vincent Gerbouin
Source: https://www.pexels.com/photo/brown-nipa-hut-on-body-of-water-1179156/

Author: eberhard grossgasteiger
Source: https://www.pexels.com/photo/architecture-autumn-conifer-dawn-629168/

Author: Pixabay
Source: https://www.pexels.com/photo/action-adult-art-beauty-267961/

--------------------
Slider
--------------------
Author: Vinicius Wiesehofer
Source: https://www.pexels.com/photo/woman-wearing-blue-button-up-jacket-1130625/

--------------------
Below Slider
--------------------
Author: Mídia
Source: https://www.pexels.com/photo/basketball-player-holding-ball-974502/

Author: Darcy Lawrey
Source: https://www.pexels.com/photo/two-man-riding-mountain-bike-on-dirt-road-at-daytime-1010557/

Author: Pixabay
Source: https://www.pexels.com/photo/ball-court-design-game-209977/

Author: Trinity Kubassek
Source: https://www.pexels.com/photo/action-active-blur-close-up-457496/

Advertisement Banner
Author: Studio 7042
Source: https://www.pexels.com/photo/white-smartphone-beside-silver-laptop-computer-1036808/


Logo and Advertisement Banner image are both self created/or edited and Licensed under GPLv3 or later

Images used for demo content can be found at https://demo.themeinwp.com/infinity-mag/sources-and-credits/

All are Licensed under CC0


== Google Fonts ==
Rubik
Source: https://fonts.google.com/specimen/Rubik
Designer: Hubert and Fischer, Meir Sadan, Cyreal
License: Open Font License

Roboto
Source: https://fonts.google.com/specimen/Roboto
Designer: Christian Robertson
License: Apache License, Version 2.0


== Changelog ==

= 1-0.1 - =
=Added social share plugin on recomendation and fixed some css =

= 1-0.0 - =
=Initial release=

