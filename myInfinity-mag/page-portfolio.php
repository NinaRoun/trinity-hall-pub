<?php  //on this page we can make any changes, e.g. remov title. For naming this file we also could use page-id.php, e.g. page-14.php. So, we make individual content for any page connected to the tabs on navbar-header

get_header();

  if (have_posts()):
    while(have_posts()): the_post(); ?>

        <article class="post page">
        	
          <!-- column container -->
          <div class="column-container clearfix">
            <div class="title-column">
              <h2><?php the_title(); ?></h2>
            </div>
            <div class="text-column">
              <?php the_content(); ?>
            </div>
          </div><!-- /column container -->
          <!--  see style-css: two column title layout  -->
        </article>
        
    <?php endwhile;
      else:
        echo '<p>No content found</p>'

      endif;

  get_footer();
  
?>