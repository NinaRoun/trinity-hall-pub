<?php  //loop through the posts to output them

get_header(); ?>

  <!-- site content -->
  <div class="site-content clearfix">

    <?php if(have_posts()) :
      while (have_posts()) : the_post();

        the_content();

      endwhile;

    else: 
      echo '<p>No content found</p>';

    endif;
    ?>

    <div class="row clearfix mt-4 categPlusNews">
      <div class="col-md-6">
        <!-- particular category posts loop begins here -->

        <?php $categoryPosts = new WP_Query('cat=4&posts_per_page=2');//object that retains the category with id=4, posts are sorted by date by default. Also this line may look like "('cat=4&posts_per_page=2&orderby=title&order=ASC');" ASC may be changed for DESC

        if($categoryPosts -> have_posts()) :
          while ($categoryPosts -> have_posts()) : $categoryPosts -> the_post(); ?>

          <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
          <?php the_excerpt(); ?>

          <?php endwhile;

        else: 
          echo '<p>No content found</p>';

        endif;
        wp_reset_postdata();//prevents our custom WP_Query loops from disrupting main natural url-based loops that WP runs on its own
        ?>
      </div>

      <div class="col-md-6">
        
        <!-- news posts loop begins here -->
    
        <?php $newsPosts = new WP_Query('page=73&posts_per_page=2');//object that retains the category with id=4, posts are sorted by date by default. Also this line may look like "('cat=4&posts_per_page=2&orderby=title&order=ASC');" ASC may be changed for DESC

        if($newsPosts -> have_posts()) :
          while ($newsPosts -> have_posts()) : $newsPosts -> the_post(); ?>

            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <?php the_excerpt(); ?>

          <?php endwhile;

        else: 
          echo '<p>No content found</p>';

        endif;
        wp_reset_postdata();//prevents our custom WP_Query loops from disrupting main natural url-based loops that WP runs on its own
        ?>
      </div>

    </div> <!-- /posts row -->

  </div><!-- /site content -->
  
  <?php get_footer();
  ?>

