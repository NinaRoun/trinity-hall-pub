<?php  //for some pages to have common layout different from other pages layout

/* Template Name: Special Layout
*/
//we go to admin panel -> template (to the right) -> from default template to special layout
//Info-box will be on the right side of the page

get_header();

  if (have_posts()):
    while(have_posts()): the_post(); ?>

        <article class="post">
          <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          </h2>

          <!-- info-box -->
          <div class="info-box">
            <h4>Disclaimer Title</h4>
            <p>Some dummy text. Some dummy text. Some dummy text. Some dummy text. Some dummy text. Some dummy text. Some dummy text. </p>
          </div>
          <!-- add some css -->

          <?php the_content(); ?>
        </article>
        
    <?php endwhile;
      else:
        echo '<p>No content found</p>'

      endif;

  get_footer();
  
?>