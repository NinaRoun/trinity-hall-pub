WordPress Website on a localhost using XAMPP

1. Setup

-install XAMPP (instructions for Linux)
Download the XAMPP package from its official page https://www.apachefriends.org/download.html.

- run XAMPP Installer
$ chmod +x xampp-file-name-here.run (e.g.: $ chmod +x xampp-linux-x64-7.2.11-0-installer.run)
$ sudo ./xampp-file-name-here.run

XAMPP will be installed at /opt/lampp.

- create a new project (lampp -> htdocs -> create new Folder (in case of experiencing any problems, copy the "wordpress" folder and rename it), move to step 2 - setup data from the database then continue

-put the theme from the repository (myInfinity-mag) to the theme folder (htdocs -> created project -> wp-content -> themes)

- run the Manager
$ sudo opt/lampp/manager-linux-x64.run

- as a window of XAMPP manager appear, activate Apache and MySQL

- type http://localhost/project-name

2. Setup data from the database

- type http://localhost//phpMyAdmin, create a new database

- go to the folder with the project, open wp-config.php (or rename wp-config-sample.php to wp-config.php), look for the line "...'DB_NAME', '...'", put the name of the created database

- define ("DB_USER", 'root'), define ("DB_PASSWORD", '')

- import the database from the repository and continue with step 1
