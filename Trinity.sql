-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 15, 2019 at 04:11 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Trinity`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-10-11 11:01:38', '2018-10-11 11:01:38', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/trinityPubProject', 'yes'),
(2, 'home', 'http://localhost/trinityPubProject', 'yes'),
(3, 'blogname', 'Trinity Hall Irish Pub', 'yes'),
(4, 'blogdescription', 'Refresh your inner Irish!', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'nina_roun@mail.ru', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '', 'yes'),
(29, 'rewrite_rules', '', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:3:{i:0;s:80:\"/opt/lampp/htdocs/trinityPubProject/wp-content/themes/myInfinity-mag/sidebar.php\";i:1;s:78:\"/opt/lampp/htdocs/trinityPubProject/wp-content/themes/myInfinity-mag/style.css\";i:2;s:0:\"\";}', 'no'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:4:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}i:3;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}i:4;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '73', 'yes'),
(84, 'page_on_front', '71', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:4:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}i:3;a:3:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;s:9:\"show_date\";b:0;}i:4;a:3:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;s:9:\"show_date\";b:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:5:{s:10:\"slide-menu\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-1\";a:2:{i:0;s:12:\"categories-3\";i:1;s:14:\"recent-posts-3\";}s:19:\"wp_inactive_widgets\";a:0:{}s:8:\"sidebar1\";a:2:{i:0;s:12:\"categories-4\";i:1;s:14:\"recent-posts-4\";}s:13:\"array_version\";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'cron', 'a:7:{i:1547564625;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1547570851;a:1:{s:18:\"ai1wm_cleanup_cron\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1547593309;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1547618873;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1547633025;a:1:{s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1547636514;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(110, 'secure_auth_key', ';GTA4G/G,u/5qVPw(gmyimDf<%1H?D3|%+.l/3zZ#4aY4`M5$OK7b(.aFxou]/D<', 'no'),
(111, 'secure_auth_salt', '?{T0ZPj(--8S3unJf(&R}cm|GTu`zw?mKq*kd5Q@Vq67=`*Wt>/{0X.lrr_G(ty#', 'no'),
(112, 'logged_in_key', 'j4;Zu/,z?w?5[DTI4yQtHiP!HRpLkic.P.-zDHZTYx8hAJazJ4qIqhDq6$1H/.cS', 'no'),
(113, 'logged_in_salt', 'l!L5;p[gJRS$aGvyLbI=&.VBQQ|r*5u|M%>X;HD`1zV^N%7~2HUWn r^xm,S7)(_', 'no'),
(114, 'nonce_key', 'u)e*POTi1idk/zcw&+(r]6..nb*2zCT/wx/;PB@dg,wLRE_V9|.5},3_hi]L7$#Z', 'no'),
(115, 'nonce_salt', 'P1f{Orpx`:3`g5NEDH#?2D;1:Sx-C;m|!{FNuvml}o LHf<z+I$anb7j/u$)dCWU', 'no'),
(123, 'can_compress_scripts', '0', 'no'),
(141, 'current_theme', 'Infinity Mag', 'yes'),
(142, 'theme_mods_marte', 'a:3:{i:0;b:0;s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1539256199;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(143, 'theme_switched', '', 'yes'),
(147, 'theme_mods_new-magazine', 'a:3:{i:0;b:0;s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1539256892;s:4:\"data\";a:9:{s:19:\"wp_inactive_widgets\";a:0:{}s:10:\"slide-menu\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:14:\"top-header-add\";N;s:9:\"sidebar-1\";N;s:14:\"sidebar-home-1\";N;s:14:\"sidebar-home-2\";N;s:14:\"footer-col-one\";N;s:14:\"footer-col-two\";N;s:16:\"footer-col-three\";N;}}}', 'yes'),
(148, 'widget_infinity-mag-grid-layout', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(149, 'widget_infinity-mag-list-layout', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(150, 'widget_infinity-mag-alternate-layout', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(151, 'widget_infinity-mag-popular-sidebar-layout', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(152, 'widget_infinity-mag-carousel-layout', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(153, 'widget_infinity-mag-author-layout', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(154, 'widget_infinity-mag-tabbed', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(155, 'widget_infinity-mag-social-layout', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(156, 'widget_infinity_mag-slider-layout', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(160, 'theme_mods_infinity-mag', 'a:12:{i:0;b:0;s:18:\"custom_css_post_id\";i:-1;s:18:\"nav_menu_locations\";a:3:{s:7:\"primary\";i:2;s:6:\"footer\";i:3;s:6:\"social\";i:0;}s:16:\"background_image\";s:0:\"\";s:17:\"background_preset\";s:3:\"fit\";s:21:\"background_position_x\";s:4:\"left\";s:21:\"background_position_y\";s:3:\"top\";s:15:\"background_size\";s:7:\"contain\";s:17:\"background_repeat\";s:9:\"no-repeat\";s:21:\"background_attachment\";s:5:\"fixed\";s:16:\"background_color\";s:6:\"5f9ea0\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1540477736;s:4:\"data\";a:9:{s:19:\"wp_inactive_widgets\";a:0:{}s:10:\"slide-menu\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:14:\"top-header-add\";N;s:9:\"sidebar-1\";N;s:14:\"sidebar-home-1\";N;s:14:\"sidebar-home-2\";N;s:14:\"footer-col-one\";N;s:14:\"footer-col-two\";N;s:16:\"footer-col-three\";N;}}}', 'yes'),
(168, 'theme_mods_newTheme2018', 'a:3:{i:0;b:0;s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1539330797;s:4:\"data\";a:9:{s:19:\"wp_inactive_widgets\";a:0:{}s:10:\"slide-menu\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:14:\"top-header-add\";N;s:9:\"sidebar-1\";N;s:14:\"sidebar-home-1\";N;s:14:\"sidebar-home-2\";N;s:14:\"footer-col-one\";N;s:14:\"footer-col-two\";N;s:16:\"footer-col-three\";N;}}}', 'yes'),
(172, 'WPLANG', '', 'yes'),
(181, 'theme_mods_twentyseventeen', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1539330808;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(185, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(205, '_transient_infinity_mag_categories', '1', 'yes'),
(208, 'theme_mods_myInfinity-mag', 'a:11:{i:0;b:0;s:18:\"nav_menu_locations\";a:3:{s:7:\"primary\";i:2;s:6:\"footer\";i:3;s:6:\"social\";i:0;}s:18:\"custom_css_post_id\";i:-1;s:14:\"lwp_link_color\";s:7:\"#660053\";s:13:\"lwp_btn_color\";s:7:\"#e9967a\";s:19:\"lwp_linkHover_color\";s:7:\"#000b1e\";s:27:\"lwp-footer-callout-headline\";s:19:\"Offer of the Month!\";s:23:\"lwp-footer-callout-text\";s:228:\"Especially for our local and regular guests a thank you program that serves up a new offer around the 17th of every month that can be redeemed before the start of the following month! \nClick to see all benefits for our Guests...\";s:23:\"lwp-footer-callout-link\";s:2:\"73\";s:24:\"lwp-footer-callout-image\";i:96;s:26:\"lwp-footer-callout-display\";s:3:\"Yes\";}', 'yes'),
(245, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:4:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.3.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.0.3-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.0.3-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.0.3\";s:7:\"version\";s:5:\"5.0.3\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.3.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.0.3-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.0.3-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.0.3\";s:7:\"version\";s:5:\"5.0.3\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.0.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.0.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.0.2\";s:7:\"version\";s:5:\"5.0.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.9.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.9.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.9-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.9-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-4.9.9-partial-8.zip\";s:8:\"rollback\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.9-rollback-8.zip\";}s:7:\"current\";s:5:\"4.9.9\";s:7:\"version\";s:5:\"4.9.9\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:5:\"4.9.8\";s:9:\"new_files\";s:0:\"\";}}s:12:\"last_checked\";i:1547562421;s:15:\"version_checked\";s:5:\"4.9.8\";s:12:\"translations\";a:0:{}}', 'no'),
(246, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(271, 'recently_activated', 'a:0:{}', 'yes'),
(276, 'auth_key', 'jX!0NG$xuxt G9i_9=JvNH|7a4^@mi:]L:8RhqP0RhS?598$O/=+<k5o|ZlveJ*p', 'no'),
(277, 'auth_salt', '<6#mRbGd RR{r}!!5PRglVI%:.Q3ePDw2]rB*XK8S=?p=YF]ZVWW)uKg3n-5I$UQ', 'no'),
(279, 'active_plugins', 'a:1:{i:0;s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";}', 'yes'),
(280, 'ai1wm_secret_key', '2hDtY8pC6l9Q', 'yes'),
(281, 'ai1wm_status', 'a:2:{s:4:\"type\";s:8:\"download\";s:7:\"message\";s:223:\"<a href=\"http://localhost/trinityPubProject/wp-content/ai1wm-backups/localhost-trinitypubproject-20190115-145817-986.wpress\" class=\"ai1wm-button-green ai1wm-emphasize\"><span>Download localhost</span><em>Size: 33 MB</em></a>\";}', 'yes'),
(287, 'ai1wm_updater', 'a:0:{}', 'yes'),
(288, 'template', 'myInfinity-mag', 'yes'),
(289, 'stylesheet', 'myInfinity-mag', 'yes'),
(290, 'jetpack_active_modules', 'a:0:{}', 'yes'),
(314, 'category_children', 'a:0:{}', 'yes'),
(332, '_site_transient_timeout_theme_roots', '1547564223', 'no'),
(333, '_site_transient_theme_roots', 'a:7:{s:12:\"infinity-mag\";s:7:\"/themes\";s:5:\"marte\";s:7:\"/themes\";s:14:\"myInfinity-mag\";s:7:\"/themes\";s:12:\"new-magazine\";s:7:\"/themes\";s:13:\"twentyfifteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no'),
(334, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1547562425;s:7:\"checked\";a:7:{s:12:\"infinity-mag\";s:5:\"1.0.1\";s:5:\"marte\";s:6:\"3.0.12\";s:14:\"myInfinity-mag\";s:3:\"1.0\";s:12:\"new-magazine\";s:5:\"1.0.0\";s:13:\"twentyfifteen\";s:3:\"1.8\";s:15:\"twentyseventeen\";s:3:\"1.3\";s:13:\"twentysixteen\";s:3:\"1.3\";}s:8:\"response\";a:4:{s:5:\"marte\";a:4:{s:5:\"theme\";s:5:\"marte\";s:11:\"new_version\";s:6:\"3.0.14\";s:3:\"url\";s:35:\"https://wordpress.org/themes/marte/\";s:7:\"package\";s:54:\"https://downloads.wordpress.org/theme/marte.3.0.14.zip\";}s:13:\"twentyfifteen\";a:4:{s:5:\"theme\";s:13:\"twentyfifteen\";s:11:\"new_version\";s:3:\"2.3\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentyfifteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentyfifteen.2.3.zip\";}s:15:\"twentyseventeen\";a:4:{s:5:\"theme\";s:15:\"twentyseventeen\";s:11:\"new_version\";s:3:\"2.0\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentyseventeen/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentyseventeen.2.0.zip\";}s:13:\"twentysixteen\";a:4:{s:5:\"theme\";s:13:\"twentysixteen\";s:11:\"new_version\";s:3:\"1.8\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentysixteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentysixteen.1.8.zip\";}}s:12:\"translations\";a:0:{}}', 'no'),
(335, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1547562427;s:7:\"checked\";a:3:{s:19:\"akismet/akismet.php\";s:5:\"3.3.3\";s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";s:4:\"6.79\";s:9:\"hello.php\";s:3:\"1.6\";}s:8:\"response\";a:2:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:3:\"4.1\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:54:\"https://downloads.wordpress.org/plugin/akismet.4.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.0.3\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:37:\"w.org/plugins/all-in-one-wp-migration\";s:4:\"slug\";s:23:\"all-in-one-wp-migration\";s:6:\"plugin\";s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";s:11:\"new_version\";s:4:\"6.82\";s:3:\"url\";s:54:\"https://wordpress.org/plugins/all-in-one-wp-migration/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/all-in-one-wp-migration.6.82.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/all-in-one-wp-migration/assets/icon-256x256.png?rev=1985064\";s:2:\"1x\";s:76:\"https://ps.w.org/all-in-one-wp-migration/assets/icon-128x128.png?rev=1985064\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:79:\"https://ps.w.org/all-in-one-wp-migration/assets/banner-1544x500.png?rev=1985064\";s:2:\"1x\";s:78:\"https://ps.w.org/all-in-one-wp-migration/assets/banner-772x250.png?rev=1985064\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.0.2\";s:12:\"requires_php\";s:6:\"5.2.17\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:1:{s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(336, '_transient_timeout_plugin_slugs', '1547650612', 'no'),
(337, '_transient_plugin_slugs', 'a:3:{i:0;s:19:\"akismet/akismet.php\";i:1;s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";i:2;s:9:\"hello.php\";}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 1, '_edit_lock', '1541677213:1'),
(3, 1, '_edit_last', '1'),
(5, 1, 'infinity-mag-meta-select-layout', 'right-sidebar'),
(6, 1, 'infinity-mag-meta-image-layout', 'full'),
(7, 5, '_edit_last', '1'),
(8, 5, '_edit_lock', '1541785478:1'),
(10, 5, 'infinity-mag-meta-select-layout', 'right-sidebar'),
(11, 5, 'infinity-mag-meta-image-layout', 'full'),
(12, 7, '_edit_last', '1'),
(13, 7, '_edit_lock', '1540894603:1'),
(14, 7, 'infinity-mag-meta-select-layout', 'right-sidebar'),
(15, 7, 'infinity-mag-meta-image-layout', 'full'),
(16, 9, '_edit_last', '1'),
(17, 9, '_edit_lock', '1539335999:1'),
(18, 9, 'infinity-mag-meta-select-layout', 'right-sidebar'),
(19, 9, 'infinity-mag-meta-image-layout', 'full'),
(20, 11, '_edit_last', '1'),
(21, 11, '_edit_lock', '1539336016:1'),
(22, 11, 'infinity-mag-meta-select-layout', 'right-sidebar'),
(23, 11, 'infinity-mag-meta-image-layout', 'full'),
(24, 13, '_edit_last', '1'),
(25, 13, '_edit_lock', '1539336039:1'),
(26, 13, 'infinity-mag-meta-select-layout', 'right-sidebar'),
(27, 13, 'infinity-mag-meta-image-layout', 'full'),
(28, 15, '_menu_item_type', 'post_type'),
(29, 15, '_menu_item_menu_item_parent', '0'),
(30, 15, '_menu_item_object_id', '13'),
(31, 15, '_menu_item_object', 'page'),
(32, 15, '_menu_item_target', ''),
(33, 15, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(34, 15, '_menu_item_xfn', ''),
(35, 15, '_menu_item_url', ''),
(36, 15, '_menu_item_orphaned', '1539336200'),
(37, 16, '_menu_item_type', 'post_type'),
(38, 16, '_menu_item_menu_item_parent', '0'),
(39, 16, '_menu_item_object_id', '11'),
(40, 16, '_menu_item_object', 'page'),
(41, 16, '_menu_item_target', ''),
(42, 16, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(43, 16, '_menu_item_xfn', ''),
(44, 16, '_menu_item_url', ''),
(46, 17, '_menu_item_type', 'post_type'),
(47, 17, '_menu_item_menu_item_parent', '0'),
(48, 17, '_menu_item_object_id', '9'),
(49, 17, '_menu_item_object', 'page'),
(50, 17, '_menu_item_target', ''),
(51, 17, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(52, 17, '_menu_item_xfn', ''),
(53, 17, '_menu_item_url', ''),
(55, 18, '_menu_item_type', 'post_type'),
(56, 18, '_menu_item_menu_item_parent', '0'),
(57, 18, '_menu_item_object_id', '7'),
(58, 18, '_menu_item_object', 'page'),
(59, 18, '_menu_item_target', ''),
(60, 18, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(61, 18, '_menu_item_xfn', ''),
(62, 18, '_menu_item_url', ''),
(64, 19, '_menu_item_type', 'post_type'),
(65, 19, '_menu_item_menu_item_parent', '0'),
(66, 19, '_menu_item_object_id', '13'),
(67, 19, '_menu_item_object', 'page'),
(68, 19, '_menu_item_target', ''),
(69, 19, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(70, 19, '_menu_item_xfn', ''),
(71, 19, '_menu_item_url', ''),
(73, 20, '_menu_item_type', 'post_type'),
(74, 20, '_menu_item_menu_item_parent', '0'),
(75, 20, '_menu_item_object_id', '11'),
(76, 20, '_menu_item_object', 'page'),
(77, 20, '_menu_item_target', ''),
(78, 20, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(79, 20, '_menu_item_xfn', ''),
(80, 20, '_menu_item_url', ''),
(82, 21, '_menu_item_type', 'post_type'),
(83, 21, '_menu_item_menu_item_parent', '0'),
(84, 21, '_menu_item_object_id', '9'),
(85, 21, '_menu_item_object', 'page'),
(86, 21, '_menu_item_target', ''),
(87, 21, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(88, 21, '_menu_item_xfn', ''),
(89, 21, '_menu_item_url', ''),
(91, 22, '_menu_item_type', 'post_type'),
(92, 22, '_menu_item_menu_item_parent', '0'),
(93, 22, '_menu_item_object_id', '7'),
(94, 22, '_menu_item_object', 'page'),
(95, 22, '_menu_item_target', ''),
(96, 22, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(97, 22, '_menu_item_xfn', ''),
(98, 22, '_menu_item_url', ''),
(100, 23, '_wp_attached_file', '2018/10/iconLogoo.png'),
(101, 23, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:340;s:6:\"height\";i:340;s:4:\"file\";s:21:\"2018/10/iconLogoo.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"iconLogoo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"iconLogoo-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:20:\"infinity-mag-400-260\";a:4:{s:4:\"file\";s:21:\"iconLogoo-340x260.png\";s:5:\"width\";i:340;s:6:\"height\";i:260;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(102, 23, '_wp_attachment_is_custom_background', 'infinity-mag'),
(105, 26, '_edit_last', '1'),
(106, 26, '_edit_lock', '1541086599:1'),
(107, 26, '_wp_page_template', 'default'),
(108, 28, '_edit_last', '1'),
(109, 28, '_edit_lock', '1541086635:1'),
(110, 28, '_wp_page_template', 'default'),
(111, 30, '_edit_last', '1'),
(112, 30, '_edit_lock', '1541086520:1'),
(113, 30, '_wp_page_template', 'default'),
(114, 7, '_wp_page_template', 'default'),
(119, 1, '_wp_old_slug', 'hello-world'),
(125, 47, '_wp_attached_file', '2018/10/happyBirthdayRocket.jpg'),
(126, 47, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:564;s:6:\"height\";i:789;s:4:\"file\";s:31:\"2018/10/happyBirthdayRocket.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"happyBirthdayRocket-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"happyBirthdayRocket-214x300.jpg\";s:5:\"width\";i:214;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(127, 48, '_wp_attached_file', '2018/10/geburtstag.jpg'),
(128, 48, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:379;s:6:\"height\";i:240;s:4:\"file\";s:22:\"2018/10/geburtstag.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"geburtstag-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"geburtstag-300x190.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:190;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(129, 49, '_wp_attached_file', '2018/10/geburtstag-1.jpg'),
(130, 49, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:379;s:6:\"height\";i:240;s:4:\"file\";s:24:\"2018/10/geburtstag-1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"geburtstag-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"geburtstag-1-300x190.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:190;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(131, 50, '_wp_attached_file', '2018/10/geburtstag-2.jpg'),
(132, 50, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:379;s:6:\"height\";i:240;s:4:\"file\";s:24:\"2018/10/geburtstag-2.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"geburtstag-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"geburtstag-2-300x190.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:190;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(133, 51, '_wp_attached_file', '2018/10/geburtstag-3.jpg'),
(134, 51, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:379;s:6:\"height\";i:240;s:4:\"file\";s:24:\"2018/10/geburtstag-3.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"geburtstag-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"geburtstag-3-300x190.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:190;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(135, 52, '_wp_attached_file', '2018/10/geburtstag-4.jpg'),
(136, 52, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:379;s:6:\"height\";i:240;s:4:\"file\";s:24:\"2018/10/geburtstag-4.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"geburtstag-4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"geburtstag-4-300x190.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:190;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(137, 53, '_wp_attached_file', '2018/11/Screenshot-from-2018-09-04-13-46-05.png'),
(138, 53, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:653;s:6:\"height\";i:348;s:4:\"file\";s:47:\"2018/11/Screenshot-from-2018-09-04-13-46-05.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:47:\"Screenshot-from-2018-09-04-13-46-05-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:47:\"Screenshot-from-2018-09-04-13-46-05-300x160.png\";s:5:\"width\";i:300;s:6:\"height\";i:160;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(141, 65, '_edit_last', '1'),
(142, 65, '_edit_lock', '1541156751:1'),
(151, 1, '_thumbnail_id', '52'),
(155, 71, '_edit_last', '1'),
(156, 71, '_edit_lock', '1541771268:1'),
(157, 71, '_wp_page_template', 'default'),
(158, 73, '_edit_last', '1'),
(159, 73, '_wp_page_template', 'default'),
(160, 73, '_edit_lock', '1541783705:1'),
(161, 75, '_menu_item_type', 'post_type'),
(162, 75, '_menu_item_menu_item_parent', '0'),
(163, 75, '_menu_item_object_id', '73'),
(164, 75, '_menu_item_object', 'page'),
(165, 75, '_menu_item_target', ''),
(166, 75, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(167, 75, '_menu_item_xfn', ''),
(168, 75, '_menu_item_url', ''),
(170, 78, '_edit_last', '1'),
(171, 78, '_edit_lock', '1541786477:1'),
(173, 80, '_edit_last', '1'),
(174, 80, '_edit_lock', '1547564067:1'),
(180, 86, '_wp_attached_file', '2018/11/irishCoffee.jpeg'),
(181, 86, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:616;s:6:\"height\";i:462;s:4:\"file\";s:24:\"2018/11/irishCoffee.jpeg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"irishCoffee-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"irishCoffee-300x225.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"small-thumbnail\";a:4:{s:4:\"file\";s:24:\"irishCoffee-180x120.jpeg\";s:5:\"width\";i:180;s:6:\"height\";i:120;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(182, 80, '_thumbnail_id', '86'),
(203, 95, '_wp_attached_file', '2018/11/offerOfMonth.jpg'),
(204, 95, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:348;s:6:\"height\";i:348;s:4:\"file\";s:24:\"2018/11/offerOfMonth.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"offerOfMonth-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"offerOfMonth-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"small-thumbnail\";a:4:{s:4:\"file\";s:24:\"offerOfMonth-180x120.jpg\";s:5:\"width\";i:180;s:6:\"height\";i:120;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(205, 95, '_edit_lock', '1542034343:1'),
(206, 95, '_edit_last', '1'),
(207, 96, '_wp_attached_file', '2018/11/cropped-offerOfMonth.jpg'),
(208, 96, '_wp_attachment_context', 'lwp-footer-callout-image-control'),
(209, 96, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:500;s:4:\"file\";s:32:\"2018/11/cropped-offerOfMonth.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"cropped-offerOfMonth-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"cropped-offerOfMonth-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"small-thumbnail\";a:4:{s:4:\"file\";s:32:\"cropped-offerOfMonth-180x120.jpg\";s:5:\"width\";i:180;s:6:\"height\";i:120;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-10-11 11:01:38', '2018-10-11 11:01:38', '<h2 class=\"font_2\" style=\"text-align: center;\">You Say Its Your Birthday?</h2>\r\n<p class=\"font_7\"><span class=\"color_18\">Discount Promotion Available Sundays ONLY</span></p>\r\n<p class=\"font_7\"><span class=\"color_18\">(Valid On the Sunday before  OR after your birthday this year!)</span></p>\r\n<p class=\"font_7\">Come celebrate with a party at the pub, and bring all your friends and family!  Trinity Hall offers a unique Birthday Promotion that can save the party up to $150!<!--more--></p>\r\n\r\n<ul class=\"font_7\">\r\n 	<li>\r\n<p class=\"font_7\">Your Age as a % Discount Off your Party\'s Food Check</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">No limit on the number of guests!</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Discount offered on Sunday Before or After Birthdate</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Alcohol is not discounted</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Party can bring their favorite Birthday Cake we\'ll cut &amp; serve</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Maximum Discount is limited to $15 per Adult ordering Sandwich or Entree, child menu items are ineligible.</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Discount is applied to a single check however payments may be split between guests.</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">We DO NOT add any automatic gratuities, and respectfully remind parties to consider the pre-discounted total when deciding on any tip for your servers.</p>\r\n</li>\r\n</ul>', 'Birthday Deal!', '', 'publish', 'open', 'open', '', 'birthday-deal', '', '', '2018-11-08 11:21:38', '2018-11-08 11:21:38', '', 0, 'http://localhost/trinityPubProject/?p=1', 0, 'post', '', 1),
(2, 1, '2018-10-11 11:01:38', '2018-10-11 11:01:38', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href=\"http://localhost/trinityPubProject/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2018-10-11 11:01:38', '2018-10-11 11:01:38', '', 0, 'http://localhost/trinityPubProject/?page_id=2', 0, 'page', '', 0),
(4, 1, '2018-10-12 06:07:46', '2018-10-12 06:07:46', '<h2 class=\"font_2\">You Say Its Your Birthday?</h2>\r\n<p class=\"font_7\"><span class=\"color_18\">Discount Promotion Available Sundays ONLY</span></p>\r\n<p class=\"font_7\"><span class=\"color_18\">(Valid On the Sunday before  OR after your birthday this year!)</span></p>\r\n<p class=\"font_7\">      Come celebrate with a party at the pub, and bring all your friends and family!  Trinity Hall offers a unique Birthday Promotion that can save the party up to $150!</p>\r\n\r\n<ul class=\"font_7\">\r\n 	<li>\r\n<p class=\"font_7\">Your Age as a % Discount Off your Party\'s Food Check</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">No limit on the number of guests!</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Discount offered on Sunday Before or After Birthdate</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Alcohol is not discounted</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Party can bring their favorite Birthday Cake we\'ll cut &amp; serve</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Maximum Discount is limited to $15 per Adult ordering Sandwich or Entree, child menu items are ineligible.</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Discount is applied to a single check however payments may be split between guests.</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">We DO NOT add any automatic gratuities, and respectfully remind parties to consider the pre-discounted total when deciding on any tip for your servers.</p>\r\n</li>\r\n</ul>', 'Birthday Deal!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-10-12 06:07:46', '2018-10-12 06:07:46', '', 1, 'http://localhost/trinityPubProject/?p=4', 0, 'revision', '', 0),
(5, 1, '2018-10-12 06:09:38', '2018-10-12 06:09:38', '<h5 class=\"font_8\" style=\"text-align: left;\">Sundays 7:30pm! What is Pub Quiz Trivia and How Does It Play?</h5>\r\n<p class=\"font_7\">Also known as a \"Table Quiz\"  or \"Trivia Night\", Trinity Hall\'s Sunday Trivia is considered one of the most challenging in town! We kick off to a full house almost every Sunday at 7:30 PM, QuizMaster Roger brings 4 rounds of 12 questions that include a nice mix of Politics, Movies, Geography, Sports, Current Events and General Trivia in each round.<!--more--></p>\r\n<p class=\"font_7\">There\'s also a handout each week with pictures or puzzles to solve during the evening, and a unique progressive Bonus question, offered to one lucky team - answer it correctly and take home the cash, miss it and the pot grows for next week!  Teams as big as your table, lots of fun and family friendly. Gather your best team and come play this Sunday evening, its free to play &amp; prizes are awarded!</p>', 'Pub Quiz Trivia', '', 'publish', 'open', 'open', '', 'pub-quiz-trivia', '', '', '2018-11-09 17:46:40', '2018-11-09 17:46:40', '', 0, 'http://localhost/trinityPubProject/?p=5', 0, 'post', '', 0),
(6, 1, '2018-10-12 06:09:38', '2018-10-12 06:09:38', '<h2 class=\"font_2\">Sundays 7:30PM!</h2>\r\n<p class=\"font_8\">What is Pub Quiz Trivia and How Does It Play?</p>\r\n<p class=\"font_7\"> Also known as a \"Table Quiz\"  or \"Trivia Night\", Trinity Hall\'s Sunday Trivia is considered one of the most challenging in town! We kick off to a full house almost every Sunday at 7:30 PM, QuizMaster Roger brings 4 rounds of 12 questions that include a nice mix of Politics, Movies, Geography, Sports, Current Events and General Trivia in each round.</p>\r\n<p class=\"font_7\">        There\'s also a handout each week with pictures or puzzles to solve during the evening, and a unique progressive Bonus question, offered to one lucky team - answer it correctly and take home the cash, miss it and the pot grows for next week!  Teams as big as your table, lots of fun and family friendly. Gather your best team and come play this Sunday evening, its free to play &amp; prizes are awarded!</p>', 'Pub Quiz Trivia', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-10-12 06:09:38', '2018-10-12 06:09:38', '', 5, 'http://localhost/trinityPubProject/?p=6', 0, 'revision', '', 0),
(7, 1, '2018-10-12 09:20:24', '2018-10-12 09:20:24', '<h5>\".... Less about the place you are from, and more about the person you are! Your inner  Irish is about enjoying yourself in the company of friends, putting down the phones and picking up the conversations! Trinity Hall Irish Pub will help you find it with a great beer selection, the best whiskey list in Dallas and a friendly staff that enjoys taking care of you!\"</h5>', 'About Us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2018-10-29 06:32:53', '2018-10-29 06:32:53', '', 0, 'http://localhost/trinityPubProject/?page_id=7', 0, 'page', '', 0),
(8, 1, '2018-10-12 09:20:24', '2018-10-12 09:20:24', 'About us info', 'About Us', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-10-12 09:20:24', '2018-10-12 09:20:24', '', 7, 'http://localhost/trinityPubProject/?p=8', 0, 'revision', '', 0),
(9, 1, '2018-10-12 09:22:18', '2018-10-12 09:22:18', 'menu 1, menu 2', 'Menus', '', 'publish', 'closed', 'closed', '', 'menus', '', '', '2018-10-12 09:22:18', '2018-10-12 09:22:18', '', 0, 'http://localhost/trinityPubProject/?page_id=9', 0, 'page', '', 0),
(10, 1, '2018-10-12 09:22:18', '2018-10-12 09:22:18', 'menu 1, menu 2', 'Menus', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2018-10-12 09:22:18', '2018-10-12 09:22:18', '', 9, 'http://localhost/trinityPubProject/?p=10', 0, 'revision', '', 0),
(11, 1, '2018-10-12 09:22:37', '2018-10-12 09:22:37', 'Some bands', 'Music', '', 'publish', 'closed', 'closed', '', 'music', '', '', '2018-10-12 09:22:37', '2018-10-12 09:22:37', '', 0, 'http://localhost/trinityPubProject/?page_id=11', 0, 'page', '', 0),
(12, 1, '2018-10-12 09:22:37', '2018-10-12 09:22:37', 'Some bands', 'Music', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2018-10-12 09:22:37', '2018-10-12 09:22:37', '', 11, 'http://localhost/trinityPubProject/?p=12', 0, 'revision', '', 0),
(13, 1, '2018-10-12 09:22:57', '2018-10-12 09:22:57', 'Invite to work', 'Jobs', '', 'publish', 'closed', 'closed', '', 'jobs', '', '', '2018-10-12 09:22:57', '2018-10-12 09:22:57', '', 0, 'http://localhost/trinityPubProject/?page_id=13', 0, 'page', '', 0),
(14, 1, '2018-10-12 09:22:57', '2018-10-12 09:22:57', 'Invite to work', 'Jobs', '', 'inherit', 'closed', 'closed', '', '13-revision-v1', '', '', '2018-10-12 09:22:57', '2018-10-12 09:22:57', '', 13, 'http://localhost/trinityPubProject/?p=14', 0, 'revision', '', 0),
(15, 1, '2018-10-12 09:23:20', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-10-12 09:23:20', '0000-00-00 00:00:00', '', 0, 'http://localhost/trinityPubProject/?p=15', 1, 'nav_menu_item', '', 0),
(16, 1, '2018-10-12 09:30:42', '2018-10-12 09:30:42', ' ', '', '', 'publish', 'closed', 'closed', '', '16', '', '', '2018-11-09 14:04:05', '2018-11-09 14:04:05', '', 0, 'http://localhost/trinityPubProject/?p=16', 4, 'nav_menu_item', '', 0),
(17, 1, '2018-10-12 09:30:42', '2018-10-12 09:30:42', ' ', '', '', 'publish', 'closed', 'closed', '', '17', '', '', '2018-11-09 14:04:05', '2018-11-09 14:04:05', '', 0, 'http://localhost/trinityPubProject/?p=17', 3, 'nav_menu_item', '', 0),
(18, 1, '2018-10-12 09:30:42', '2018-10-12 09:30:42', ' ', '', '', 'publish', 'closed', 'closed', '', '18', '', '', '2018-11-09 14:04:05', '2018-11-09 14:04:05', '', 0, 'http://localhost/trinityPubProject/?p=18', 1, 'nav_menu_item', '', 0),
(19, 1, '2018-10-12 09:33:52', '2018-10-12 09:33:52', ' ', '', '', 'publish', 'closed', 'closed', '', '19', '', '', '2018-10-12 09:33:52', '2018-10-12 09:33:52', '', 0, 'http://localhost/trinityPubProject/?p=19', 4, 'nav_menu_item', '', 0),
(20, 1, '2018-10-12 09:33:52', '2018-10-12 09:33:52', ' ', '', '', 'publish', 'closed', 'closed', '', '20', '', '', '2018-10-12 09:33:52', '2018-10-12 09:33:52', '', 0, 'http://localhost/trinityPubProject/?p=20', 3, 'nav_menu_item', '', 0),
(21, 1, '2018-10-12 09:33:52', '2018-10-12 09:33:52', ' ', '', '', 'publish', 'closed', 'closed', '', '21', '', '', '2018-10-12 09:33:52', '2018-10-12 09:33:52', '', 0, 'http://localhost/trinityPubProject/?p=21', 2, 'nav_menu_item', '', 0),
(22, 1, '2018-10-12 09:33:52', '2018-10-12 09:33:52', ' ', '', '', 'publish', 'closed', 'closed', '', '22', '', '', '2018-10-12 09:33:52', '2018-10-12 09:33:52', '', 0, 'http://localhost/trinityPubProject/?p=22', 1, 'nav_menu_item', '', 0),
(23, 1, '2018-10-12 09:42:42', '2018-10-12 09:42:42', '', 'iconLogoo', '', 'inherit', 'open', 'closed', '', 'iconlogoo', '', '', '2018-10-12 09:42:42', '2018-10-12 09:42:42', '', 0, 'http://localhost/trinityPubProject/wp-content/uploads/2018/10/iconLogoo.png', 0, 'attachment', 'image/png', 0),
(26, 1, '2018-10-29 05:58:05', '2018-10-29 05:58:05', '<div id=\"comp-iv12gj43\" class=\"txtNew\" data-packed=\"true\">\r\n<h4 class=\"font_4\" style=\"text-align: center;\">We are THE place to watch Rugby in DFW!</h4>\r\n</div>\r\n<div id=\"comp-jebph7xk\" class=\"txtNew\" data-packed=\"false\" data-min-height=\"747\">\r\n<p class=\"font_7\">We sometimes miss a fixture coming up and if you know a match we could show, please drop us a line or call the pub! We subscribe to NBC Gold Rugby, TheRugbyChannel, ESPN+, and also host Pay Per View events with Premium Sports.</p>\r\n<!--more-->\r\n<p class=\"font_7\" style=\"text-align: center;\"><strong><span class=\"color_18\">RUGBY CHAMPIONSHIP 2018</span></strong></p>\r\n<p class=\"font_7\" style=\"text-align: center;\">New Zealand, Australia, South Africa &amp; Argentina</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">12 match tournament</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">8/18   Aus V NZ            8:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">           S.Afr V Arg       10:00A  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">8/25   NZ  V Aus           8:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">          Arg V S.Afr          2:10P LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">9/8     NZ V Arg            8:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">         Aus V S.Afr        10:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">9/15  NZ  V S.Afr          9:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">         Aus V  Arg         10:50A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">9/29 S.Afr V Aus        10:00A  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">         Arg  V  NZ            5:40P  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">10/6 S.Afr V NZ          10:00A  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">          Arg V Aus            5:40P  LIVE</p>\r\n<p class=\"font_7\"> (Replay indicates match was overnight when we were closed and we will open to show it as a replay, so..... NO SPOILERS!)</p>\r\n\r\n</div>', 'Rugby', '', 'publish', 'closed', 'closed', '', 'rugby', '', '', '2018-11-01 15:38:58', '2018-11-01 15:38:58', '', 7, 'http://localhost/trinityPubProject/?page_id=26', 0, 'page', '', 0),
(27, 1, '2018-10-29 05:58:05', '2018-10-29 05:58:05', '<div id=\"comp-iv12gj43\" class=\"txtNew\" data-packed=\"true\">\r\n<h4 class=\"font_4\">We are THE place to watch Rugby in DFW!</h4>\r\n</div>\r\n<div id=\"comp-jebph7xk\" class=\"txtNew\" data-packed=\"false\" data-min-height=\"747\">\r\n<p class=\"font_7\">We sometimes miss a fixture coming up and if you know a match we could show, please drop us a line or call the pub! We subscribe to NBC Gold Rugby, TheRugbyChannel, ESPN+, and also host Pay Per View events with Premium Sports.</p>\r\n<p class=\"font_7\" style=\"text-align: center;\"><strong><span class=\"color_18\">RUGBY CHAMPIONSHIP 2018</span></strong></p>\r\n<p class=\"font_7\" style=\"text-align: center;\">New Zealand, Australia, South Africa &amp; Argentina</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">12 match tournament</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">8/18   Aus V NZ            8:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">           S.Afr V Arg       10:00A  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">8/25   NZ  V Aus           8:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">          Arg V S.Afr          2:10P LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">9/8     NZ V Arg            8:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">         Aus V S.Afr        10:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">9/15  NZ  V S.Afr          9:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">         Aus V  Arg         10:50A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">9/29 S.Afr V Aus        10:00A  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">         Arg  V  NZ            5:40P  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">10/6 S.Afr V NZ          10:00A  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">          Arg V Aus            5:40P  LIVE</p>\r\n<p class=\"font_7\"></p>\r\n<p class=\"font_7\"> (Replay indicates match was overnight when we were closed and we will open to show it as a replay, so..... NO SPOILERS!)</p>\r\n<p class=\"font_7\"></p>\r\n\r\n</div>', 'RUGBY', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2018-10-29 05:58:05', '2018-10-29 05:58:05', '', 26, 'http://localhost/trinityPubProject/?p=27', 0, 'revision', '', 0),
(28, 1, '2018-10-29 06:18:24', '2018-10-29 06:18:24', '<div id=\"comp-ik4sh3l8\" class=\"txtNew\" data-packed=\"true\">\r\n<h2 class=\"font_2\" style=\"text-align: center;\"><span class=\"color_18\">TV MATCHES</span></h2>\r\n</div>\r\n<div id=\"comp-iv47dyqz\" class=\"txtNew\" data-packed=\"true\">\r\n<p class=\"font_7\">            EPL, La Liga, Bundesliga, Scottish FA, Internationals, MLS?</p>\r\n<p class=\"font_7\">            We Watch Them All &amp; Invite Our Guests To Enjoy Them With Us!</p>\r\n<p class=\"font_7\">            If You\'re Coming To F &amp; Blind, Get In A Row or Give Some Other Fan A Hard Time.... Choose Another Pub, You\'ll Enjoy It More</p>\r\n<p style=\"font-size: 16px;\">            We\'re Family Friendly where Moms can bring their Boys, &amp; Daughters can bring Dad! So aside from an occasional &amp; involuntary exclamation - which can happen, even to a bishop, once..... -  our guests are fans of the game &amp; treat each other with respect. Fans that prefer not to allow the rest of us to enjoy the match will be asked to leave.</p>\r\n<!--more-->\r\n<p style=\"font-size: 16px; text-align: center;\"><strong>NEW TO BEING A FAN OF FOOTBALL in DALLAS? </strong></p>\r\n<p style=\"font-size: 16px;\">            Watched in a pub that was all Hargy Bargy, Push &amp; Shove, FNBlind?  Come along and watch with us where Moms can bring their sons and Dads their daughters to enjoy the beautiful game among passionate fans who understand the difference between the occasional surprising outburst and a 90-minute rant at the Ref, Team, League &amp; passers-by!  Come Watch With Us &amp; Enjoy The Game!</p>\r\n<p style=\"font-size: 16px;\">            OTHER CLUBS MEET: Man City - British Lion (Frisco)  Arsenal,  Liverpool , Newcastle United- Londoner ( Addison)  West Ham - McSwiggans  (The Colony)  Manchester United - Henderson TapHouse (Dallas) Chelsea FC - BBC - (Dallas), AFC Bournemouth  AKA ...THE CHERRIES! - Trinity Hall Right Here!</p>\r\n<p style=\"text-align: center;\"><strong>Thanks for watching with us!</strong></p>\r\n<p style=\"font-size: 16px;\">            Fixtures are posted up to 2 weeks in advance, and may contain errors or incorrect information, most will be updated and corrected before game day! If you are not sure about something you see or read, please call the Pub for clarification!</p>\r\n<p style=\"font-size: 16px;\">           Bar service on Saturday mornings starts at 7AM  Sunday\'s alcohol service available at 10AM, when served with food or Noon if no food has been purchased! It is illegal to bring alcohol into or remove it from a permitted outlet. <span class=\"wixGuard\">​</span></p>\r\n<p style=\"font-size: 16px;\">We may be here &amp; open before bar permit allows sale of alcohol, please keep in mind we follow state law.</p>\r\n\r\n</div>', 'Football', '', 'publish', 'closed', 'closed', '', 'football', '', '', '2018-11-01 15:39:33', '2018-11-01 15:39:33', '', 7, 'http://localhost/trinityPubProject/?page_id=28', 0, 'page', '', 0),
(29, 1, '2018-10-29 06:18:24', '2018-10-29 06:18:24', '<div id=\"comp-ik4sh3l8\" class=\"txtNew\" data-packed=\"true\">\r\n<h2 class=\"font_2\"><span class=\"color_18\">TV MATCHES</span></h2>\r\n</div>\r\n<div id=\"comp-iv47dyqz\" class=\"txtNew\" data-packed=\"true\">\r\n<p class=\"font_7\">EPL, La Liga, Bundesliga, Scottish FA, Internationals, MLS?</p>\r\n<p class=\"font_7\">We Watch Them All &amp; Invite Our Guests To Enjoy Them With Us!</p>\r\n<p class=\"font_7\">If You\'re Coming To F &amp; Blind, Get In A Row or Give Some Other Fan A Hard Time.... Choose Another Pub, You\'ll Enjoy It More</p>\r\n<p class=\"font_7\">We\'re Family Friendly where Moms can bring their Boys, &amp; Daughters can bring Dad! So aside from an occasional &amp; involuntary exclamation - which can happen, even to a bishop, once..... -  our guests are fans of the game &amp; treat each other with respect. Fans that prefer not to allow the rest of us to enjoy the match will be asked to leave.</p>\r\n<p class=\"font_7\"><strong>NEW TO BEING A FAN OF FOOTBALL in DALLAS? </strong></p>\r\n<p class=\"font_7\">Watched in a pub that was all Hargy Bargy, Push &amp; Shove, FNBlind?  Come along and watch with us where Moms can bring their sons and Dads their daughters to enjoy the beautiful game among passionate fans who understand the difference between the occasional surprising outburst and a 90-minute rant at the Ref, Team, League &amp; passers-by!  Come Watch With Us &amp; Enjoy The Game!</p>\r\n<p class=\"font_7\">OTHER CLUBS MEET: Man City - British Lion (Frisco)  Arsenal,  Liverpool , Newcastle United- Londoner ( Addison)  West Ham - McSwiggans  (The Colony)  Manchester United - Henderson TapHouse (Dallas) Chelsea FC - BBC - (Dallas), AFC Bournemouth  AKA ...THE CHERRIES! - Trinity Hall Right Here!</p>\r\n<p class=\"font_7\" style=\"text-align: center;\"><strong>Thanks for watching with us!</strong></p>\r\n<p class=\"font_7\">            Fixtures are posted up to 2 weeks in advance, and may contain errors or incorrect information, most will be updated and corrected before game day! If you are not sure about something you see or read, please call the Pub for clarification!</p>\r\n<p class=\"font_7\">           Bar service on Saturday mornings starts at 7AM  Sunday\'s alcohol service available at 10AM, when served with food or Noon if no food has been purchased! It is illegal to bring alcohol into or remove it from a permitted outlet. <span class=\"wixGuard\">​</span></p>\r\n<p class=\"font_7\">We may be here &amp; open before bar permit allows sale of alcohol, please keep in mind we follow state law.</p>\r\n\r\n</div>', 'FOOTBALL', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2018-10-29 06:18:24', '2018-10-29 06:18:24', '', 28, 'http://localhost/trinityPubProject/?p=29', 0, 'revision', '', 0),
(30, 1, '2018-10-29 06:20:49', '2018-10-29 06:20:49', '<div id=\"ie66k6iu\" class=\"txtNew\" data-packed=\"false\" data-min-height=\"245\">\r\n<h2 class=\"font_2\" style=\"text-align: center;\">Texas Holdem Poker</h2>\r\n<p class=\"font_8\" style=\"text-align: left;\">       Our weekly Pub game is a family friendly, table focused, no limit game of hold em, dealt by the players, with blinds raised as decided by the table. Its not the place for hoodies, mirror glasses or ear buds, rather a pint, a smile and some laughs are what we hope you enjoy!</p>\r\n<p class=\"font_8\" dir=\"ltr\">      <span class=\"color_18\">  </span><span class=\"color_18\">OVERVIEW :</span> Our usual Monday Night we will have 3 or 4 tables of 8 players, all will have signed up at the bar by 7:15PM to give us time to balance table counts and bring out more chips if needed. We ask everyone to be seated or standing beside their table within 10 minutes of the 1st deal. Finals seating arrangements will be at the Pub\'s sole approval as we attempt to balance the number of players per game.</p>\r\n<p class=\"font_8\" dir=\"ltr\">        When you sign in, you\'ll read about our Beer &amp; Food Specials that are offered every week, with upto $2 off featured drafts and $3 off featured menu entrees.</p>\r\n\r\n</div>\r\n<!--more-->\r\n<div id=\"ie69tfmw\" class=\"txtNew\" data-packed=\"false\" data-min-height=\"332\">\r\n<p class=\"font_8\" dir=\"ltr\">         <span class=\"color_18\">PLAY :</span>      Game 1.  plays each Monday at 7:30 PM  and last hand at 9:00PM.  The top six chip holders in the room will move on to the final table, they will be joined by 2 players from the previous week.</p>\r\n<p class=\"font_8\" dir=\"ltr\">        Game 2. Deals in at 9:30PM those guests that are not on the finals table may play on the consolation table(s) which play through 11PM. Then the top 2 chip holders on Consolation tables will be eligible for the following week\'s Final Table!</p>\r\n<p class=\"font_8\" dir=\"ltr\">        We begin each game with new set of chips and do not carry over chips from table to table, nor do we refill tables. Every house has their rules and so do we, and first is do no harm to the game. This means questions about a call, a bet or a house rule are answered by house, and generally not in mid game. Specific issues that have been addressed will be posted below for reference. If it hasn\'t been played here that way before, its not starting tonight! Suggestions for improving the play or action are always appreciated and can be submitted attached to a $20 Bill to the Pit Boss.</p>\r\n\r\n</div>\r\n<div id=\"ie6a3lmt\" class=\"txtNew\" data-packed=\"true\">\r\n<p class=\"font_8\" dir=\"ltr\"><span class=\"color_18\">        WEEKLY PRIZES </span>:</p>\r\n<p class=\"font_8\" dir=\"ltr\">       1st $30*</p>\r\n<p class=\"font_8\" dir=\"ltr\">            2nd $25*</p>\r\n<p class=\"font_8\" dir=\"ltr\">                     3rd $20*</p>\r\n<p class=\"font_8\" dir=\"ltr\">                             4th $15*</p>\r\n<p class=\"font_8\" dir=\"ltr\">       *Top 4 Chip holders on the Finals Table each week will receive gift certifcates that are good at Trinity Hall on the winner\'s next visit.</p>\r\n<p class=\"font_8\" dir=\"ltr\">If you enjoy playing free poker online, and want to play around real people that are nice, or people that are real nice, you may have found your game!</p>\r\n<p class=\"font_8\" dir=\"ltr\">        First hands are dealt at 7:30PM &amp; 9:30PM sharp, to avoid missing the deal and losing your seat we suggest joining us early to sign in and then to stay close to your assigned table 10 Minutes before game starts. If you want to refresh your knowledge on winning hands and how we play Pub Poker please download our <a href=\"http://docs.wixstatic.com/ugd/36fbba_ae2a77f05d9f497ab3e9e84467a0901b.pdf\" target=\"_blank\" rel=\"noopener\" data-type=\"document\"><span class=\"color_3\">Pub Poker Rules!</span></a> <a href=\"http://docs.wixstatic.com/ugd/36fbba_e0c48e8efdf140888b9af626b65935aa.pdf\" target=\"_blank\" rel=\"noopener\" data-type=\"document\">to start. Additional items addressed recently include:</a></p>\r\n<p class=\"font_8\" dir=\"ltr\">             Mis-deal becomes Burn Card everytime, there is no choice for player, when misdeal occurs during dealing of 1st cards all players cards are burned and new cards are dealt, if dealer is on 2nd and subsequent cards when mis deal occurs it is only the players; mis dealt card that is burned.</p>\r\n<p class=\"font_8\" dir=\"ltr\">            Straddle Bets are not permitted, due to the variants in this bet, each of which impacts different players and the betting procedure ether by round or hand, we do not and its low level of use by recreational players, we have chosen to not permit its use.</p>\r\n<p class=\"font_8\" dir=\"ltr\">            Birthday Cakes served during the game night must be offered to all players without concern as to what their chip count is. <span class=\"wixGuard\">​</span></p>\r\n<p class=\"font_8\" dir=\"ltr\">            The Provost\'s Screen will be reserved for showing Dancing With The Stars when the program is in season, except on those weeks in which national security, national championships or players at Table #1 do not object to a different program unanimously .</p>\r\n<p class=\"font_8\" dir=\"ltr\">             Comfort &amp; care for any horses ridden in  the Pub for the game,  is the responsibility of the player/rider and not the Pub Staff and should be resolved before sitting down at the table!</p>\r\n<p class=\"font_8\" dir=\"ltr\">             The Pub may at its own and sole discretion  remove a player from the game at any time and for any reason we may also refuse entry to the game</p>\r\n<p class=\"font_8\" dir=\"ltr\"><span class=\"wixGuard\">​</span></p>\r\n\r\n</div>', 'Pocker Night', '', 'publish', 'closed', 'closed', '', 'pocker-night', '', '', '2018-11-01 15:37:40', '2018-11-01 15:37:40', '', 7, 'http://localhost/trinityPubProject/?page_id=30', 0, 'page', '', 0),
(31, 1, '2018-10-29 06:20:49', '2018-10-29 06:20:49', '<div id=\"ie66k6iu\" class=\"txtNew\" data-packed=\"false\" data-min-height=\"245\">\r\n<h2 class=\"font_2\" style=\"text-align: center;\">Texas Holdem Poker</h2>\r\n<p class=\"font_8\" style=\"text-align: left;\">       Our weekly Pub game is a family friendly, table focused, no limit game of hold em, dealt by the players, with blinds raised as decided by the table. Its not the place for hoodies, mirror glasses or ear buds, rather a pint, a smile and some laughs are what we hope you enjoy!</p>\r\n<p class=\"font_8\" dir=\"ltr\">      <span class=\"color_18\">  </span><span class=\"color_18\">OVERVIEW :</span> Our usual Monday Night we will have 3 or 4 tables of 8 players, all will have signed up at the bar by 7:15PM to give us time to balance table counts and bring out more chips if needed. We ask everyone to be seated or standing beside their table within 10 minutes of the 1st deal. Finals seating arrangements will be at the Pub\'s sole approval as we attempt to balance the number of players per game.</p>\r\n<p class=\"font_8\" dir=\"ltr\">        When you sign in, you\'ll read about our Beer &amp; Food Specials that are offered every week, with upto $2 off featured drafts and $3 off featured menu entrees</p>\r\n\r\n</div>\r\n<div id=\"ie69tfmw\" class=\"txtNew\" data-packed=\"false\" data-min-height=\"332\">\r\n<p class=\"font_8\" dir=\"ltr\">         <span class=\"color_18\">PLAY :</span>      Game 1.  plays each Monday at 7:30 PM  and last hand at 9:00PM.  The top six chip holders in the room will move on to the final table, they will be joined by 2 players from the previous week.</p>\r\n<p class=\"font_8\" dir=\"ltr\">        Game 2. Deals in at 9:30PM those guests that are not on the finals table may play on the consolation table(s) which play through 11PM. Then the top 2 chip holders on Consolation tables will be eligible for the following week\'s Final Table!</p>\r\n<p class=\"font_8\" dir=\"ltr\">        We begin each game with new set of chips and do not carry over chips from table to table, nor do we refill tables. Every house has their rules and so do we, and first is do no harm to the game. This means questions about a call, a bet or a house rule are answered by house, and generally not in mid game. Specific issues that have been addressed will be posted below for reference. If it hasn\'t been played here that way before, its not starting tonight! Suggestions for improving the play or action are always appreciated and can be submitted attached to a $20 Bill to the Pit Boss.</p>\r\n\r\n</div>\r\n<div id=\"ie6a3lmt\" class=\"txtNew\" data-packed=\"true\">\r\n<p class=\"font_8\" dir=\"ltr\"><span class=\"color_18\">        WEEKLY PRIZES </span>:</p>\r\n<p class=\"font_8\" dir=\"ltr\">       1st $30*</p>\r\n<p class=\"font_8\" dir=\"ltr\">            2nd $25*</p>\r\n<p class=\"font_8\" dir=\"ltr\">                     3rd $20*</p>\r\n<p class=\"font_8\" dir=\"ltr\">                             4th $15*</p>\r\n<p class=\"font_8\" dir=\"ltr\">       *Top 4 Chip holders on the Finals Table each week will receive gift certifcates that are good at Trinity Hall on the winner\'s next visit.</p>\r\n<p class=\"font_8\" dir=\"ltr\">If you enjoy playing free poker online, and want to play around real people that are nice, or people that are real nice, you may have found your game!</p>\r\n<p class=\"font_8\" dir=\"ltr\">        First hands are dealt at 7:30PM &amp; 9:30PM sharp, to avoid missing the deal and losing your seat we suggest joining us early to sign in and then to stay close to your assigned table 10 Minutes before game starts. If you want to refresh your knowledge on winning hands and how we play Pub Poker please download our <a href=\"http://docs.wixstatic.com/ugd/36fbba_ae2a77f05d9f497ab3e9e84467a0901b.pdf\" target=\"_blank\" rel=\"noopener\" data-type=\"document\"><span class=\"color_3\">Pub Poker Rules!</span></a> <a href=\"http://docs.wixstatic.com/ugd/36fbba_e0c48e8efdf140888b9af626b65935aa.pdf\" target=\"_blank\" rel=\"noopener\" data-type=\"document\">to start. Additional items addressed recently include:</a></p>\r\n<p class=\"font_8\" dir=\"ltr\">             Mis-deal becomes Burn Card everytime, there is no choice for player, when misdeal occurs during dealing of 1st cards all players cards are burned and new cards are dealt, if dealer is on 2nd and subsequent cards when mis deal occurs it is only the players; mis dealt card that is burned.</p>\r\n<p class=\"font_8\" dir=\"ltr\">            Straddle Bets are not permitted, due to the variants in this bet, each of which impacts different players and the betting procedure ether by round or hand, we do not and its low level of use by recreational players, we have chosen to not permit its use.</p>\r\n<p class=\"font_8\" dir=\"ltr\">            Birthday Cakes served during the game night must be offered to all players without concern as to what their chip count is. <span class=\"wixGuard\">​</span></p>\r\n<p class=\"font_8\" dir=\"ltr\">            The Provost\'s Screen will be reserved for showing Dancing With The Stars when the program is in season, except on those weeks in which national security, national championships or players at Table #1 do not object to a different program unanimously .</p>\r\n<p class=\"font_8\" dir=\"ltr\">             Comfort &amp; care for any horses ridden in  the Pub for the game,  is the responsibility of the player/rider and not the Pub Staff and should be resolved before sitting down at the table!</p>\r\n<p class=\"font_8\" dir=\"ltr\">             The Pub may at its own and sole discretion  remove a player from the game at any time and for any reason we may also refuse entry to the game</p>\r\n<p class=\"font_8\" dir=\"ltr\"><span class=\"wixGuard\">​</span></p>\r\n\r\n</div>', 'POCKER NIGHT', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2018-10-29 06:20:49', '2018-10-29 06:20:49', '', 30, 'http://localhost/trinityPubProject/?p=31', 0, 'revision', '', 0),
(32, 1, '2018-10-29 06:26:21', '2018-10-29 06:26:21', '<div id=\"comp-iv12gj43\" class=\"txtNew\" data-packed=\"true\">\r\n<h4 class=\"font_4\">We are THE place to watch Rugby in DFW!</h4>\r\n</div>\r\n<div id=\"comp-jebph7xk\" class=\"txtNew\" data-packed=\"false\" data-min-height=\"747\">\r\n<p class=\"font_7\">We sometimes miss a fixture coming up and if you know a match we could show, please drop us a line or call the pub! We subscribe to NBC Gold Rugby, TheRugbyChannel, ESPN+, and also host Pay Per View events with Premium Sports.</p>\r\n<p class=\"font_7\" style=\"text-align: center;\"><strong><span class=\"color_18\">RUGBY CHAMPIONSHIP 2018</span></strong></p>\r\n<p class=\"font_7\" style=\"text-align: center;\">New Zealand, Australia, South Africa &amp; Argentina</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">12 match tournament</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">8/18   Aus V NZ            8:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">           S.Afr V Arg       10:00A  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">8/25   NZ  V Aus           8:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">          Arg V S.Afr          2:10P LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">9/8     NZ V Arg            8:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">         Aus V S.Afr        10:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">9/15  NZ  V S.Afr          9:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">         Aus V  Arg         10:50A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">9/29 S.Afr V Aus        10:00A  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">         Arg  V  NZ            5:40P  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">10/6 S.Afr V NZ          10:00A  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">          Arg V Aus            5:40P  LIVE</p>\r\n<p class=\"font_7\"> (Replay indicates match was overnight when we were closed and we will open to show it as a replay, so..... NO SPOILERS!)</p>\r\n\r\n</div>', 'RUGBY', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2018-10-29 06:26:21', '2018-10-29 06:26:21', '', 26, 'http://localhost/trinityPubProject/?p=32', 0, 'revision', '', 0),
(33, 1, '2018-10-29 06:31:45', '2018-10-29 06:31:45', '\".... Less about the place you are from, and more about the person you are! Your inner  Irish is about enjoying yourself in the company of friends, putting down the phones and picking up the conversations! Trinity Hall Irish Pub will help you find it with a great beer selection, the best whiskey list in Dallas and a friendly staff that enjoys taking care of you!\"', 'About Us', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-10-29 06:31:45', '2018-10-29 06:31:45', '', 7, 'http://localhost/trinityPubProject/?p=33', 0, 'revision', '', 0),
(34, 1, '2018-10-29 06:32:47', '2018-10-29 06:32:47', '<h5>\".... Less about the place you are from, and more about the person you are! Your inner  Irish is about enjoying yourself in the company of friends, putting down the phones and picking up the conversations! Trinity Hall Irish Pub will help you find it with a great beer selection, the best whiskey list in Dallas and a friendly staff that enjoys taking care of you!\"</h5>', 'About Us', '', 'inherit', 'closed', 'closed', '', '7-autosave-v1', '', '', '2018-10-29 06:32:47', '2018-10-29 06:32:47', '', 7, 'http://localhost/trinityPubProject/?p=34', 0, 'revision', '', 0),
(35, 1, '2018-10-29 06:32:53', '2018-10-29 06:32:53', '<h5>\".... Less about the place you are from, and more about the person you are! Your inner  Irish is about enjoying yourself in the company of friends, putting down the phones and picking up the conversations! Trinity Hall Irish Pub will help you find it with a great beer selection, the best whiskey list in Dallas and a friendly staff that enjoys taking care of you!\"</h5>', 'About Us', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-10-29 06:32:53', '2018-10-29 06:32:53', '', 7, 'http://localhost/trinityPubProject/?p=35', 0, 'revision', '', 0),
(36, 1, '2018-10-29 06:35:29', '2018-10-29 06:35:29', '<div id=\"comp-ik4sh3l8\" class=\"txtNew\" data-packed=\"true\">\r\n<h2 class=\"font_2\"><span class=\"color_18\">TV MATCHES</span></h2>\r\n</div>\r\n<div id=\"comp-iv47dyqz\" class=\"txtNew\" data-packed=\"true\">\r\n<p class=\"font_7\">EPL, La Liga, Bundesliga, Scottish FA, Internationals, MLS?</p>\r\n<p class=\"font_7\">We Watch Them All &amp; Invite Our Guests To Enjoy Them With Us!</p>\r\n<p class=\"font_7\">If You\'re Coming To F &amp; Blind, Get In A Row or Give Some Other Fan A Hard Time.... Choose Another Pub, You\'ll Enjoy It More</p>\r\n<p>We\'re Family Friendly where Moms can bring their Boys, &amp; Daughters can bring Dad! So aside from an occasional &amp; involuntary exclamation - which can happen, even to a bishop, once..... -  our guests are fans of the game &amp; treat each other with respect. Fans that prefer not to allow the rest of us to enjoy the match will be asked to leave.</p>\r\n<p class=\"font_7\"><strong>NEW TO BEING A FAN OF FOOTBALL in DALLAS? </strong></p>\r\n<p class=\"font_7\">Watched in a pub that was all Hargy Bargy, Push &amp; Shove, FNBlind?  Come along and watch with us where Moms can bring their sons and Dads their daughters to enjoy the beautiful game among passionate fans who understand the difference between the occasional surprising outburst and a 90-minute rant at the Ref, Team, League &amp; passers-by!  Come Watch With Us &amp; Enjoy The Game!</p>\r\n<p class=\"font_7\">OTHER CLUBS MEET: Man City - British Lion (Frisco)  Arsenal,  Liverpool , Newcastle United- Londoner ( Addison)  West Ham - McSwiggans  (The Colony)  Manchester United - Henderson TapHouse (Dallas) Chelsea FC - BBC - (Dallas), AFC Bournemouth  AKA ...THE CHERRIES! - Trinity Hall Right Here!</p>\r\n<p class=\"font_7\" style=\"text-align: center;\"><strong>Thanks for watching with us!</strong></p>\r\n<p class=\"font_7\">            Fixtures are posted up to 2 weeks in advance, and may contain errors or incorrect information, most will be updated and corrected before game day! If you are not sure about something you see or read, please call the Pub for clarification!</p>\r\n<p class=\"font_7\">           Bar service on Saturday mornings starts at 7AM  Sunday\'s alcohol service available at 10AM, when served with food or Noon if no food has been purchased! It is illegal to bring alcohol into or remove it from a permitted outlet. <span class=\"wixGuard\">​</span></p>\r\n<p class=\"font_7\">We may be here &amp; open before bar permit allows sale of alcohol, please keep in mind we follow state law.</p>\r\n\r\n</div>', 'FOOTBALL', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2018-10-29 06:35:29', '2018-10-29 06:35:29', '', 28, 'http://localhost/trinityPubProject/?p=36', 0, 'revision', '', 0),
(37, 1, '2018-10-29 06:36:23', '2018-10-29 06:36:23', '<div id=\"comp-ik4sh3l8\" class=\"txtNew\" data-packed=\"true\">\r\n<h2 class=\"font_2\"><span class=\"color_18\">TV MATCHES</span></h2>\r\n</div>\r\n<div id=\"comp-iv47dyqz\" class=\"txtNew\" data-packed=\"true\">\r\n<p class=\"font_7\">EPL, La Liga, Bundesliga, Scottish FA, Internationals, MLS?</p>\r\n<p class=\"font_7\">We Watch Them All &amp; Invite Our Guests To Enjoy Them With Us!</p>\r\n<p class=\"font_7\">If You\'re Coming To F &amp; Blind, Get In A Row or Give Some Other Fan A Hard Time.... Choose Another Pub, You\'ll Enjoy It More</p>\r\n<p style=\"font-size:20px;\">We\'re Family Friendly where Moms can bring their Boys, &amp; Daughters can bring Dad! So aside from an occasional &amp; involuntary exclamation - which can happen, even to a bishop, once..... -  our guests are fans of the game &amp; treat each other with respect. Fans that prefer not to allow the rest of us to enjoy the match will be asked to leave.</p>\r\n<p class=\"font_7\"><strong>NEW TO BEING A FAN OF FOOTBALL in DALLAS? </strong></p>\r\n<p class=\"font_7\">Watched in a pub that was all Hargy Bargy, Push &amp; Shove, FNBlind?  Come along and watch with us where Moms can bring their sons and Dads their daughters to enjoy the beautiful game among passionate fans who understand the difference between the occasional surprising outburst and a 90-minute rant at the Ref, Team, League &amp; passers-by!  Come Watch With Us &amp; Enjoy The Game!</p>\r\n<p class=\"font_7\">OTHER CLUBS MEET: Man City - British Lion (Frisco)  Arsenal,  Liverpool , Newcastle United- Londoner ( Addison)  West Ham - McSwiggans  (The Colony)  Manchester United - Henderson TapHouse (Dallas) Chelsea FC - BBC - (Dallas), AFC Bournemouth  AKA ...THE CHERRIES! - Trinity Hall Right Here!</p>\r\n<p class=\"font_7\" style=\"text-align: center;\"><strong>Thanks for watching with us!</strong></p>\r\n<p class=\"font_7\">            Fixtures are posted up to 2 weeks in advance, and may contain errors or incorrect information, most will be updated and corrected before game day! If you are not sure about something you see or read, please call the Pub for clarification!</p>\r\n<p class=\"font_7\">           Bar service on Saturday mornings starts at 7AM  Sunday\'s alcohol service available at 10AM, when served with food or Noon if no food has been purchased! It is illegal to bring alcohol into or remove it from a permitted outlet. <span class=\"wixGuard\">​</span></p>\r\n<p class=\"font_7\">We may be here &amp; open before bar permit allows sale of alcohol, please keep in mind we follow state law.</p>\r\n\r\n</div>', 'FOOTBALL', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2018-10-29 06:36:23', '2018-10-29 06:36:23', '', 28, 'http://localhost/trinityPubProject/?p=37', 0, 'revision', '', 0),
(38, 1, '2018-10-29 06:36:44', '2018-10-29 06:36:44', '<div id=\"comp-ik4sh3l8\" class=\"txtNew\" data-packed=\"true\">\r\n<h2 class=\"font_2\"><span class=\"color_18\">TV MATCHES</span></h2>\r\n</div>\r\n<div id=\"comp-iv47dyqz\" class=\"txtNew\" data-packed=\"true\">\r\n<p class=\"font_7\">EPL, La Liga, Bundesliga, Scottish FA, Internationals, MLS?</p>\r\n<p class=\"font_7\">We Watch Them All &amp; Invite Our Guests To Enjoy Them With Us!</p>\r\n<p class=\"font_7\">If You\'re Coming To F &amp; Blind, Get In A Row or Give Some Other Fan A Hard Time.... Choose Another Pub, You\'ll Enjoy It More</p>\r\n<p style=\"font-size:16px;\">We\'re Family Friendly where Moms can bring their Boys, &amp; Daughters can bring Dad! So aside from an occasional &amp; involuntary exclamation - which can happen, even to a bishop, once..... -  our guests are fans of the game &amp; treat each other with respect. Fans that prefer not to allow the rest of us to enjoy the match will be asked to leave.</p>\r\n<p class=\"font_7\"><strong>NEW TO BEING A FAN OF FOOTBALL in DALLAS? </strong></p>\r\n<p class=\"font_7\">Watched in a pub that was all Hargy Bargy, Push &amp; Shove, FNBlind?  Come along and watch with us where Moms can bring their sons and Dads their daughters to enjoy the beautiful game among passionate fans who understand the difference between the occasional surprising outburst and a 90-minute rant at the Ref, Team, League &amp; passers-by!  Come Watch With Us &amp; Enjoy The Game!</p>\r\n<p class=\"font_7\">OTHER CLUBS MEET: Man City - British Lion (Frisco)  Arsenal,  Liverpool , Newcastle United- Londoner ( Addison)  West Ham - McSwiggans  (The Colony)  Manchester United - Henderson TapHouse (Dallas) Chelsea FC - BBC - (Dallas), AFC Bournemouth  AKA ...THE CHERRIES! - Trinity Hall Right Here!</p>\r\n<p class=\"font_7\" style=\"text-align: center;\"><strong>Thanks for watching with us!</strong></p>\r\n<p class=\"font_7\">            Fixtures are posted up to 2 weeks in advance, and may contain errors or incorrect information, most will be updated and corrected before game day! If you are not sure about something you see or read, please call the Pub for clarification!</p>\r\n<p class=\"font_7\">           Bar service on Saturday mornings starts at 7AM  Sunday\'s alcohol service available at 10AM, when served with food or Noon if no food has been purchased! It is illegal to bring alcohol into or remove it from a permitted outlet. <span class=\"wixGuard\">​</span></p>\r\n<p class=\"font_7\">We may be here &amp; open before bar permit allows sale of alcohol, please keep in mind we follow state law.</p>\r\n\r\n</div>', 'FOOTBALL', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2018-10-29 06:36:44', '2018-10-29 06:36:44', '', 28, 'http://localhost/trinityPubProject/?p=38', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(39, 1, '2018-10-29 06:38:04', '2018-10-29 06:38:04', '<div id=\"comp-ik4sh3l8\" class=\"txtNew\" data-packed=\"true\">\r\n<h2 class=\"font_2\"><span class=\"color_18\">TV MATCHES</span></h2>\r\n</div>\r\n<div id=\"comp-iv47dyqz\" class=\"txtNew\" data-packed=\"true\">\r\n<p class=\"font_7\">EPL, La Liga, Bundesliga, Scottish FA, Internationals, MLS?</p>\r\n<p class=\"font_7\">We Watch Them All &amp; Invite Our Guests To Enjoy Them With Us!</p>\r\n<p class=\"font_7\">If You\'re Coming To F &amp; Blind, Get In A Row or Give Some Other Fan A Hard Time.... Choose Another Pub, You\'ll Enjoy It More</p>\r\n<p style=\"font-size:16px;\">We\'re Family Friendly where Moms can bring their Boys, &amp; Daughters can bring Dad! So aside from an occasional &amp; involuntary exclamation - which can happen, even to a bishop, once..... -  our guests are fans of the game &amp; treat each other with respect. Fans that prefer not to allow the rest of us to enjoy the match will be asked to leave.</p>\r\n<p style=\"font-size:16px;\"><strong>NEW TO BEING A FAN OF FOOTBALL in DALLAS? </strong></p>\r\n<p style=\"font-size:16px;\">Watched in a pub that was all Hargy Bargy, Push &amp; Shove, FNBlind?  Come along and watch with us where Moms can bring their sons and Dads their daughters to enjoy the beautiful game among passionate fans who understand the difference between the occasional surprising outburst and a 90-minute rant at the Ref, Team, League &amp; passers-by!  Come Watch With Us &amp; Enjoy The Game!</p>\r\n<p style=\"font-size:16px;\">OTHER CLUBS MEET: Man City - British Lion (Frisco)  Arsenal,  Liverpool , Newcastle United- Londoner ( Addison)  West Ham - McSwiggans  (The Colony)  Manchester United - Henderson TapHouse (Dallas) Chelsea FC - BBC - (Dallas), AFC Bournemouth  AKA ...THE CHERRIES! - Trinity Hall Right Here!</p>\r\n<p style=\"font-size:16px;\"\" style=\"text-align: center;\"><strong>Thanks for watching with us!</strong></p>\r\n<p style=\"font-size:16px;\">            Fixtures are posted up to 2 weeks in advance, and may contain errors or incorrect information, most will be updated and corrected before game day! If you are not sure about something you see or read, please call the Pub for clarification!</p>\r\n<p style=\"font-size:16px;\">           Bar service on Saturday mornings starts at 7AM  Sunday\'s alcohol service available at 10AM, when served with food or Noon if no food has been purchased! It is illegal to bring alcohol into or remove it from a permitted outlet. <span class=\"wixGuard\">​</span></p>\r\n<p style=\"font-size:16px;\">We may be here &amp; open before bar permit allows sale of alcohol, please keep in mind we follow state law.</p>\r\n\r\n</div>', 'FOOTBALL', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2018-10-29 06:38:04', '2018-10-29 06:38:04', '', 28, 'http://localhost/trinityPubProject/?p=39', 0, 'revision', '', 0),
(40, 1, '2018-11-01 15:35:44', '2018-11-01 15:35:44', '<div id=\"comp-ik4sh3l8\" class=\"txtNew\" data-packed=\"true\">\n<h2 class=\"font_2\" style=\"text-align: center;\"><span class=\"color_18\">TV MATCHES</span></h2>\n</div>\n<div id=\"comp-iv47dyqz\" class=\"txtNew\" data-packed=\"true\">\n<p class=\"font_7\">            EPL, La Liga, Bundesliga, Scottish FA, Internationals, MLS?</p>\n<p class=\"font_7\">            We Watch Them All &amp; Invite Our Guests To Enjoy Them With Us!</p>\n<p class=\"font_7\">            If You\'re Coming To F &amp; Blind, Get In A Row or Give Some Other Fan A Hard Time.... Choose Another Pub, You\'ll Enjoy It More</p>\n<p style=\"font-size: 16px;\">            We\'re Family Friendly where Moms can bring their Boys, &amp; Daughters can bring Dad! So aside from an occasional &amp; involuntary exclamation - which can happen, even to a bishop, once..... -  our guests are fans of the game &amp; treat each other with respect. Fans that prefer not to allow the rest of us to enjoy the match will be asked to leave.</p>\n<p style=\"font-size: 16px; text-align: center;\"><strong>NEW TO BEING A FAN OF FOOTBALL in DALLAS? </strong></p>\n<p style=\"font-size: 16px;\">            Watched in a pub that was all Hargy Bargy, Push &amp; Shove, FNBlind?  Come along and watch with us where Moms can bring their sons and Dads their daughters to enjoy the beautiful game among passionate fans who understand the difference between the occasional surprising outburst and a 90-minute rant at the Ref, Team, League &amp; passers-by!  Come Watch With Us &amp; Enjoy The Game!</p>\n<p style=\"font-size: 16px;\">            OTHER CLUBS MEET: Man City - British Lion (Frisco)  Arsenal,  Liverpool , Newcastle United- Londoner ( Addison)  West Ham - McSwiggans  (The Colony)  Manchester United - Henderson TapHouse (Dallas) Chelsea FC - BBC - (Dallas), AFC Bournemouth  AKA ...THE CHERRIES! - Trinity Hall Right Here!</p>\n<p style=\"text-align: center;\"><strong>Thanks for watching with us!</strong></p>\n<p style=\"font-size: 16px;\">            Fixtures are posted up to 2 weeks in advance, and may contain errors or incorrect information, most will be updated and corrected before game day! If you are not sure about something you see or read, please call the Pub for clarification!</p>\n<p style=\"font-size: 16px;\">           Bar service on Saturday mornings starts at 7AM  Sunday\'s alcohol service available at 10AM, when served with food or Noon if no food has been purchased! It is illegal to bring alcohol into or remove it from a permitted outlet. <span class=\"wixGuard\">​</span></p>\n<p style=\"font-size: 16px;\">We may be here &amp; open before bar permit allows sale of alcohol, please keep in mind we follow state law.</p>\n\n</div>', 'Football', '', 'inherit', 'closed', 'closed', '', '28-autosave-v1', '', '', '2018-11-01 15:35:44', '2018-11-01 15:35:44', '', 28, 'http://localhost/trinityPubProject/?p=40', 0, 'revision', '', 0),
(41, 1, '2018-10-30 10:16:03', '2018-10-30 10:16:03', '<h2 class=\"font_2\">Sundays 7:30PM!</h2>\r\n<p class=\"font_8\" style=\"text-align: center;\">What is Pub Quiz Trivia and How Does It Play?</p>\r\n<p class=\"font_7\">        Also known as a \"Table Quiz\"  or \"Trivia Night\", Trinity Hall\'s Sunday Trivia is considered one of the most challenging in town! We kick off to a full house almost every Sunday at 7:30 PM, QuizMaster Roger brings 4 rounds of 12 questions that include a nice mix of Politics, Movies, Geography, Sports, Current Events and General Trivia in each round.</p>\r\n<p class=\"font_7\">        There\'s also a handout each week with pictures or puzzles to solve during the evening, and a unique progressive Bonus question, offered to one lucky team - answer it correctly and take home the cash, miss it and the pot grows for next week!  Teams as big as your table, lots of fun and family friendly. Gather your best team and come play this Sunday evening, its free to play &amp; prizes are awarded!</p>', 'Pub Quiz Trivia', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-10-30 10:16:03', '2018-10-30 10:16:03', '', 5, 'http://localhost/trinityPubProject/?p=41', 0, 'revision', '', 0),
(42, 1, '2018-10-30 10:18:49', '2018-10-30 10:18:49', '<h2 class=\"font_2\" style=\"text-align: center;\">You Say Its Your Birthday?</h2>\r\n<p class=\"font_7\"><span class=\"color_18\">      Discount Promotion Available Sundays ONLY</span></p>\r\n<p class=\"font_7\"><span class=\"color_18\">      (Valid On the Sunday before  OR after your birthday this year!)</span></p>\r\n<p class=\"font_7\">      Come celebrate with a party at the pub, and bring all your friends and family!  Trinity Hall offers a unique Birthday Promotion that can save the party up to $150!</p>\r\n\r\n<ul class=\"font_7\">\r\n 	<li>\r\n<p class=\"font_7\">Your Age as a % Discount Off your Party\'s Food Check</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">No limit on the number of guests!</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Discount offered on Sunday Before or After Birthdate</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Alcohol is not discounted</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Party can bring their favorite Birthday Cake we\'ll cut &amp; serve</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Maximum Discount is limited to $15 per Adult ordering Sandwich or Entree, child menu items are ineligible.</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Discount is applied to a single check however payments may be split between guests.</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">We DO NOT add any automatic gratuities, and respectfully remind parties to consider the pre-discounted total when deciding on any tip for your servers.</p>\r\n</li>\r\n</ul>', 'Birthday Deal!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-10-30 10:18:49', '2018-10-30 10:18:49', '', 1, 'http://localhost/trinityPubProject/1-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2018-10-30 10:32:17', '2018-10-30 10:32:17', '<h2 class=\"font_2\" style=\"text-align: center;\">You Say Its Your Birthday?</h2>\r\n<p class=\"font_7\"><span class=\"color_18\">      Discount Promotion Available Sundays ONLY</span></p>\r\n<p class=\"font_7\"><span class=\"color_18\">      (Valid On the Sunday before  OR after your birthday this year!)</span></p>\r\n<p class=\"font_7\">      Come celebrate with a party at the pub, and bring all your friends and family!  Trinity Hall offers a unique Birthday Promotion that can save the party up to $150!</p>\r\n<!--more-->\r\n<ul class=\"font_7\">\r\n 	<li>\r\n<p class=\"font_7\">Your Age as a % Discount Off your Party\'s Food Check</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">No limit on the number of guests!</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Discount offered on Sunday Before or After Birthdate</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Alcohol is not discounted</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Party can bring their favorite Birthday Cake we\'ll cut &amp; serve</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Maximum Discount is limited to $15 per Adult ordering Sandwich or Entree, child menu items are ineligible.</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Discount is applied to a single check however payments may be split between guests.</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">We DO NOT add any automatic gratuities, and respectfully remind parties to consider the pre-discounted total when deciding on any tip for your servers.</p>\r\n</li>\r\n</ul>', 'Birthday Deal!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-10-30 10:32:17', '2018-10-30 10:32:17', '', 1, 'http://localhost/trinityPubProject/1-revision-v1/', 0, 'revision', '', 0),
(44, 1, '2018-10-30 10:32:35', '2018-10-30 10:32:35', '<h2 class=\"font_2\">Sundays 7:30PM!</h2>\r\n<p class=\"font_8\" style=\"text-align: center;\">What is Pub Quiz Trivia and How Does It Play?</p>\r\n<p class=\"font_7\">        Also known as a \"Table Quiz\"  or \"Trivia Night\", Trinity Hall\'s Sunday Trivia is considered one of the most challenging in town! We kick off to a full house almost every Sunday at 7:30 PM, QuizMaster Roger brings 4 rounds of 12 questions that include a nice mix of Politics, Movies, Geography, Sports, Current Events and General Trivia in each round.</p>\r\n<!--more-->\r\n<p class=\"font_7\">        There\'s also a handout each week with pictures or puzzles to solve during the evening, and a unique progressive Bonus question, offered to one lucky team - answer it correctly and take home the cash, miss it and the pot grows for next week!  Teams as big as your table, lots of fun and family friendly. Gather your best team and come play this Sunday evening, its free to play &amp; prizes are awarded!</p>', 'Pub Quiz Trivia', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-10-30 10:32:35', '2018-10-30 10:32:35', '', 5, 'http://localhost/trinityPubProject/5-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2018-10-30 10:33:51', '2018-10-30 10:33:51', '<h2 class=\"font_2\">Sundays 7:30PM!</h2>\r\n<p class=\"font_8\" style=\"text-align: center;\">What is Pub Quiz Trivia and How Does It Play?</p>\r\n<p class=\"font_7\">        Also known as a \"Table Quiz\"  or \"Trivia Night\", Trinity Hall\'s Sunday Trivia is considered one of the most challenging in town! We kick off to a full house almost every Sunday at 7:30 PM, QuizMaster Roger brings 4 rounds of 12 questions that include a nice mix of Politics, Movies, Geography, Sports, Current Events and General Trivia in each round.<!--more--></p>\r\n<p class=\"font_7\">        There\'s also a handout each week with pictures or puzzles to solve during the evening, and a unique progressive Bonus question, offered to one lucky team - answer it correctly and take home the cash, miss it and the pot grows for next week!  Teams as big as your table, lots of fun and family friendly. Gather your best team and come play this Sunday evening, its free to play &amp; prizes are awarded!</p>', 'Pub Quiz Trivia', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-10-30 10:33:51', '2018-10-30 10:33:51', '', 5, 'http://localhost/trinityPubProject/5-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2018-10-30 10:34:14', '2018-10-30 10:34:14', '<h2 class=\"font_2\" style=\"text-align: center;\">You Say Its Your Birthday?</h2>\r\n<p class=\"font_7\"><span class=\"color_18\">      Discount Promotion Available Sundays ONLY</span></p>\r\n<p class=\"font_7\"><span class=\"color_18\">      (Valid On the Sunday before  OR after your birthday this year!)</span></p>\r\n<p class=\"font_7\">      Come celebrate with a party at the pub, and bring all your friends and family!  Trinity Hall offers a unique Birthday Promotion that can save the party up to $150!<!--more--></p>\r\n\r\n<ul class=\"font_7\">\r\n 	<li>\r\n<p class=\"font_7\">Your Age as a % Discount Off your Party\'s Food Check</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">No limit on the number of guests!</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Discount offered on Sunday Before or After Birthdate</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Alcohol is not discounted</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Party can bring their favorite Birthday Cake we\'ll cut &amp; serve</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Maximum Discount is limited to $15 per Adult ordering Sandwich or Entree, child menu items are ineligible.</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Discount is applied to a single check however payments may be split between guests.</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">We DO NOT add any automatic gratuities, and respectfully remind parties to consider the pre-discounted total when deciding on any tip for your servers.</p>\r\n</li>\r\n</ul>', 'Birthday Deal!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-10-30 10:34:14', '2018-10-30 10:34:14', '', 1, 'http://localhost/trinityPubProject/1-revision-v1/', 0, 'revision', '', 0),
(47, 1, '2018-10-30 11:20:32', '2018-10-30 11:20:32', '', 'happyBirthdayRocket', '', 'inherit', 'open', 'closed', '', 'happybirthdayrocket', '', '', '2018-10-30 11:20:32', '2018-10-30 11:20:32', '', 1, 'http://localhost/trinityPubProject/wp-content/uploads/2018/10/happyBirthdayRocket.jpg', 0, 'attachment', 'image/jpeg', 0),
(48, 1, '2018-10-30 11:24:03', '2018-10-30 11:24:03', '', 'geburtstag', '', 'inherit', 'open', 'closed', '', 'geburtstag', '', '', '2018-10-30 11:24:03', '2018-10-30 11:24:03', '', 1, 'http://localhost/trinityPubProject/wp-content/uploads/2018/10/geburtstag.jpg', 0, 'attachment', 'image/jpeg', 0),
(49, 1, '2018-10-30 11:24:33', '2018-10-30 11:24:33', '', 'geburtstag', '', 'inherit', 'open', 'closed', '', 'geburtstag-2', '', '', '2018-10-30 11:24:33', '2018-10-30 11:24:33', '', 0, 'http://localhost/trinityPubProject/wp-content/uploads/2018/10/geburtstag-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(50, 1, '2018-10-30 11:25:50', '2018-10-30 11:25:50', '', 'geburtstag', '', 'inherit', 'open', 'closed', '', 'geburtstag-3', '', '', '2018-10-30 11:25:50', '2018-10-30 11:25:50', '', 5, 'http://localhost/trinityPubProject/wp-content/uploads/2018/10/geburtstag-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(51, 1, '2018-10-30 11:46:31', '2018-10-30 11:46:31', '', 'geburtstag', '', 'inherit', 'open', 'closed', '', 'geburtstag-4', '', '', '2018-10-30 11:46:31', '2018-10-30 11:46:31', '', 5, 'http://localhost/trinityPubProject/wp-content/uploads/2018/10/geburtstag-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(52, 1, '2018-10-30 11:48:06', '2018-10-30 11:48:06', '', 'geburtstag', '', 'inherit', 'open', 'closed', '', 'geburtstag-5', '', '', '2018-10-30 11:48:06', '2018-10-30 11:48:06', '', 1, 'http://localhost/trinityPubProject/wp-content/uploads/2018/10/geburtstag-4.jpg', 0, 'attachment', 'image/jpeg', 0),
(53, 1, '2018-11-01 09:46:00', '2018-11-01 09:46:00', '', 'Screenshot from 2018-09-04 13-46-05', '', 'inherit', 'open', 'closed', '', 'screenshot-from-2018-09-04-13-46-05', '', '', '2018-11-01 09:46:00', '2018-11-01 09:46:00', '', 0, 'http://localhost/trinityPubProject/wp-content/uploads/2018/11/Screenshot-from-2018-09-04-13-46-05.png', 0, 'attachment', 'image/png', 0),
(55, 1, '2018-11-01 10:16:20', '2018-11-01 10:16:20', '<h2 class=\"font_2\" style=\"text-align: center;\">Sundays 7:30PM!</h2>\r\n<p class=\"font_8\" style=\"text-align: left;\">        What is Pub Quiz Trivia and How Does It Play?</p>\r\n<p class=\"font_7\">        Also known as a \"Table Quiz\"  or \"Trivia Night\", Trinity Hall\'s Sunday Trivia is considered one of the most challenging in town! We kick off to a full house almost every Sunday at 7:30 PM, QuizMaster Roger brings 4 rounds of 12 questions that include a nice mix of Politics, Movies, Geography, Sports, Current Events and General Trivia in each round.<!--more--></p>\r\n<p class=\"font_7\">        There\'s also a handout each week with pictures or puzzles to solve during the evening, and a unique progressive Bonus question, offered to one lucky team - answer it correctly and take home the cash, miss it and the pot grows for next week!  Teams as big as your table, lots of fun and family friendly. Gather your best team and come play this Sunday evening, its free to play &amp; prizes are awarded!</p>', 'Pub Quiz Trivia', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-11-01 10:16:20', '2018-11-01 10:16:20', '', 5, 'http://localhost/trinityPubProject/5-revision-v1/', 0, 'revision', '', 0),
(57, 1, '2018-11-01 15:19:31', '2018-11-01 15:19:31', '<div id=\"ie66k6iu\" class=\"txtNew\" data-packed=\"false\" data-min-height=\"245\">\r\n<h2 class=\"font_2\" style=\"text-align: center;\">Texas Holdem Poker</h2>\r\n<p class=\"font_8\" style=\"text-align: left;\">       Our weekly Pub game is a family friendly, table focused, no limit game of hold em, dealt by the players, with blinds raised as decided by the table. Its not the place for hoodies, mirror glasses or ear buds, rather a pint, a smile and some laughs are what we hope you enjoy!</p>\r\n<p class=\"font_8\" dir=\"ltr\">      <span class=\"color_18\">  </span><span class=\"color_18\">OVERVIEW :</span> Our usual Monday Night we will have 3 or 4 tables of 8 players, all will have signed up at the bar by 7:15PM to give us time to balance table counts and bring out more chips if needed. We ask everyone to be seated or standing beside their table within 10 minutes of the 1st deal. Finals seating arrangements will be at the Pub\'s sole approval as we attempt to balance the number of players per game.</p>\r\n<p class=\"font_8\" dir=\"ltr\">        When you sign in, you\'ll read about our Beer &amp; Food Specials that are offered every week, with upto $2 off featured drafts and $3 off featured menu entrees.</p>\r\n\r\n</div>\r\n<!--more-->\r\n<div id=\"ie69tfmw\" class=\"txtNew\" data-packed=\"false\" data-min-height=\"332\">\r\n<p class=\"font_8\" dir=\"ltr\">         <span class=\"color_18\">PLAY :</span>      Game 1.  plays each Monday at 7:30 PM  and last hand at 9:00PM.  The top six chip holders in the room will move on to the final table, they will be joined by 2 players from the previous week.</p>\r\n<p class=\"font_8\" dir=\"ltr\">        Game 2. Deals in at 9:30PM those guests that are not on the finals table may play on the consolation table(s) which play through 11PM. Then the top 2 chip holders on Consolation tables will be eligible for the following week\'s Final Table!</p>\r\n<p class=\"font_8\" dir=\"ltr\">        We begin each game with new set of chips and do not carry over chips from table to table, nor do we refill tables. Every house has their rules and so do we, and first is do no harm to the game. This means questions about a call, a bet or a house rule are answered by house, and generally not in mid game. Specific issues that have been addressed will be posted below for reference. If it hasn\'t been played here that way before, its not starting tonight! Suggestions for improving the play or action are always appreciated and can be submitted attached to a $20 Bill to the Pit Boss.</p>\r\n\r\n</div>\r\n<div id=\"ie6a3lmt\" class=\"txtNew\" data-packed=\"true\">\r\n<p class=\"font_8\" dir=\"ltr\"><span class=\"color_18\">        WEEKLY PRIZES </span>:</p>\r\n<p class=\"font_8\" dir=\"ltr\">       1st $30*</p>\r\n<p class=\"font_8\" dir=\"ltr\">            2nd $25*</p>\r\n<p class=\"font_8\" dir=\"ltr\">                     3rd $20*</p>\r\n<p class=\"font_8\" dir=\"ltr\">                             4th $15*</p>\r\n<p class=\"font_8\" dir=\"ltr\">       *Top 4 Chip holders on the Finals Table each week will receive gift certifcates that are good at Trinity Hall on the winner\'s next visit.</p>\r\n<p class=\"font_8\" dir=\"ltr\">If you enjoy playing free poker online, and want to play around real people that are nice, or people that are real nice, you may have found your game!</p>\r\n<p class=\"font_8\" dir=\"ltr\">        First hands are dealt at 7:30PM &amp; 9:30PM sharp, to avoid missing the deal and losing your seat we suggest joining us early to sign in and then to stay close to your assigned table 10 Minutes before game starts. If you want to refresh your knowledge on winning hands and how we play Pub Poker please download our <a href=\"http://docs.wixstatic.com/ugd/36fbba_ae2a77f05d9f497ab3e9e84467a0901b.pdf\" target=\"_blank\" rel=\"noopener\" data-type=\"document\"><span class=\"color_3\">Pub Poker Rules!</span></a> <a href=\"http://docs.wixstatic.com/ugd/36fbba_e0c48e8efdf140888b9af626b65935aa.pdf\" target=\"_blank\" rel=\"noopener\" data-type=\"document\">to start. Additional items addressed recently include:</a></p>\r\n<p class=\"font_8\" dir=\"ltr\">             Mis-deal becomes Burn Card everytime, there is no choice for player, when misdeal occurs during dealing of 1st cards all players cards are burned and new cards are dealt, if dealer is on 2nd and subsequent cards when mis deal occurs it is only the players; mis dealt card that is burned.</p>\r\n<p class=\"font_8\" dir=\"ltr\">            Straddle Bets are not permitted, due to the variants in this bet, each of which impacts different players and the betting procedure ether by round or hand, we do not and its low level of use by recreational players, we have chosen to not permit its use.</p>\r\n<p class=\"font_8\" dir=\"ltr\">            Birthday Cakes served during the game night must be offered to all players without concern as to what their chip count is. <span class=\"wixGuard\">​</span></p>\r\n<p class=\"font_8\" dir=\"ltr\">            The Provost\'s Screen will be reserved for showing Dancing With The Stars when the program is in season, except on those weeks in which national security, national championships or players at Table #1 do not object to a different program unanimously .</p>\r\n<p class=\"font_8\" dir=\"ltr\">             Comfort &amp; care for any horses ridden in  the Pub for the game,  is the responsibility of the player/rider and not the Pub Staff and should be resolved before sitting down at the table!</p>\r\n<p class=\"font_8\" dir=\"ltr\">             The Pub may at its own and sole discretion  remove a player from the game at any time and for any reason we may also refuse entry to the game</p>\r\n<p class=\"font_8\" dir=\"ltr\"><span class=\"wixGuard\">​</span></p>\r\n\r\n</div>', 'POCKER NIGHT', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2018-11-01 15:19:31', '2018-11-01 15:19:31', '', 30, 'http://localhost/trinityPubProject/30-revision-v1/', 0, 'revision', '', 0),
(58, 1, '2018-11-01 15:34:42', '2018-11-01 15:34:42', '<div id=\"comp-ik4sh3l8\" class=\"txtNew\" data-packed=\"true\">\r\n<h2 class=\"font_2\"><span class=\"color_18\">TV MATCHES</span></h2>\r\n</div>\r\n<div id=\"comp-iv47dyqz\" class=\"txtNew\" data-packed=\"true\">\r\n<p class=\"font_7\">EPL, La Liga, Bundesliga, Scottish FA, Internationals, MLS?</p>\r\n<p class=\"font_7\">We Watch Them All &amp; Invite Our Guests To Enjoy Them With Us!</p>\r\n<p class=\"font_7\">If You\'re Coming To F &amp; Blind, Get In A Row or Give Some Other Fan A Hard Time.... Choose Another Pub, You\'ll Enjoy It More</p>\r\n<p style=\"font-size: 16px;\">We\'re Family Friendly where Moms can bring their Boys, &amp; Daughters can bring Dad! So aside from an occasional &amp; involuntary exclamation - which can happen, even to a bishop, once..... -  our guests are fans of the game &amp; treat each other with respect. Fans that prefer not to allow the rest of us to enjoy the match will be asked to leave.</p>\r\n<p style=\"font-size: 16px;\"><strong>NEW TO BEING A FAN OF FOOTBALL in DALLAS? </strong></p>\r\n<p style=\"font-size: 16px;\">Watched in a pub that was all Hargy Bargy, Push &amp; Shove, FNBlind?  Come along and watch with us where Moms can bring their sons and Dads their daughters to enjoy the beautiful game among passionate fans who understand the difference between the occasional surprising outburst and a 90-minute rant at the Ref, Team, League &amp; passers-by!  Come Watch With Us &amp; Enjoy The Game!</p>\r\n<p style=\"font-size: 16px;\">OTHER CLUBS MEET: Man City - British Lion (Frisco)  Arsenal,  Liverpool , Newcastle United- Londoner ( Addison)  West Ham - McSwiggans  (The Colony)  Manchester United - Henderson TapHouse (Dallas) Chelsea FC - BBC - (Dallas), AFC Bournemouth  AKA ...THE CHERRIES! - Trinity Hall Right Here!</p>\r\n<p style=\"text-align: center;\"><strong>Thanks for watching with us!</strong></p>\r\n<p style=\"font-size: 16px;\">            Fixtures are posted up to 2 weeks in advance, and may contain errors or incorrect information, most will be updated and corrected before game day! If you are not sure about something you see or read, please call the Pub for clarification!</p>\r\n<p style=\"font-size: 16px;\">           Bar service on Saturday mornings starts at 7AM  Sunday\'s alcohol service available at 10AM, when served with food or Noon if no food has been purchased! It is illegal to bring alcohol into or remove it from a permitted outlet. <span class=\"wixGuard\">​</span></p>\r\n<p style=\"font-size: 16px;\">We may be here &amp; open before bar permit allows sale of alcohol, please keep in mind we follow state law.</p>\r\n\r\n</div>', 'Football', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2018-11-01 15:34:42', '2018-11-01 15:34:42', '', 28, 'http://localhost/trinityPubProject/28-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2018-11-01 15:36:06', '2018-11-01 15:36:06', '<div id=\"comp-ik4sh3l8\" class=\"txtNew\" data-packed=\"true\">\r\n<h2 class=\"font_2\" style=\"text-align: center;\"><span class=\"color_18\">TV MATCHES</span></h2>\r\n</div>\r\n<div id=\"comp-iv47dyqz\" class=\"txtNew\" data-packed=\"true\">\r\n<p class=\"font_7\">            EPL, La Liga, Bundesliga, Scottish FA, Internationals, MLS?</p>\r\n<p class=\"font_7\">            We Watch Them All &amp; Invite Our Guests To Enjoy Them With Us!</p>\r\n<p class=\"font_7\">            If You\'re Coming To F &amp; Blind, Get In A Row or Give Some Other Fan A Hard Time.... Choose Another Pub, You\'ll Enjoy It More</p>\r\n<p style=\"font-size: 16px;\">            We\'re Family Friendly where Moms can bring their Boys, &amp; Daughters can bring Dad! So aside from an occasional &amp; involuntary exclamation - which can happen, even to a bishop, once..... -  our guests are fans of the game &amp; treat each other with respect. Fans that prefer not to allow the rest of us to enjoy the match will be asked to leave.</p>\r\n<p style=\"font-size: 16px; text-align: center;\"><strong>NEW TO BEING A FAN OF FOOTBALL in DALLAS? </strong></p>\r\n<p style=\"font-size: 16px;\">            Watched in a pub that was all Hargy Bargy, Push &amp; Shove, FNBlind?  Come along and watch with us where Moms can bring their sons and Dads their daughters to enjoy the beautiful game among passionate fans who understand the difference between the occasional surprising outburst and a 90-minute rant at the Ref, Team, League &amp; passers-by!  Come Watch With Us &amp; Enjoy The Game!</p>\r\n<p style=\"font-size: 16px;\">            OTHER CLUBS MEET: Man City - British Lion (Frisco)  Arsenal,  Liverpool , Newcastle United- Londoner ( Addison)  West Ham - McSwiggans  (The Colony)  Manchester United - Henderson TapHouse (Dallas) Chelsea FC - BBC - (Dallas), AFC Bournemouth  AKA ...THE CHERRIES! - Trinity Hall Right Here!</p>\r\n<p style=\"text-align: center;\"><strong>Thanks for watching with us!</strong></p>\r\n<p style=\"font-size: 16px;\">            Fixtures are posted up to 2 weeks in advance, and may contain errors or incorrect information, most will be updated and corrected before game day! If you are not sure about something you see or read, please call the Pub for clarification!</p>\r\n<p style=\"font-size: 16px;\">           Bar service on Saturday mornings starts at 7AM  Sunday\'s alcohol service available at 10AM, when served with food or Noon if no food has been purchased! It is illegal to bring alcohol into or remove it from a permitted outlet. <span class=\"wixGuard\">​</span></p>\r\n<p style=\"font-size: 16px;\">We may be here &amp; open before bar permit allows sale of alcohol, please keep in mind we follow state law.</p>\r\n\r\n</div>', 'Football', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2018-11-01 15:36:06', '2018-11-01 15:36:06', '', 28, 'http://localhost/trinityPubProject/28-revision-v1/', 0, 'revision', '', 0),
(60, 1, '2018-11-01 15:37:40', '2018-11-01 15:37:40', '<div id=\"ie66k6iu\" class=\"txtNew\" data-packed=\"false\" data-min-height=\"245\">\r\n<h2 class=\"font_2\" style=\"text-align: center;\">Texas Holdem Poker</h2>\r\n<p class=\"font_8\" style=\"text-align: left;\">       Our weekly Pub game is a family friendly, table focused, no limit game of hold em, dealt by the players, with blinds raised as decided by the table. Its not the place for hoodies, mirror glasses or ear buds, rather a pint, a smile and some laughs are what we hope you enjoy!</p>\r\n<p class=\"font_8\" dir=\"ltr\">      <span class=\"color_18\">  </span><span class=\"color_18\">OVERVIEW :</span> Our usual Monday Night we will have 3 or 4 tables of 8 players, all will have signed up at the bar by 7:15PM to give us time to balance table counts and bring out more chips if needed. We ask everyone to be seated or standing beside their table within 10 minutes of the 1st deal. Finals seating arrangements will be at the Pub\'s sole approval as we attempt to balance the number of players per game.</p>\r\n<p class=\"font_8\" dir=\"ltr\">        When you sign in, you\'ll read about our Beer &amp; Food Specials that are offered every week, with upto $2 off featured drafts and $3 off featured menu entrees.</p>\r\n\r\n</div>\r\n<!--more-->\r\n<div id=\"ie69tfmw\" class=\"txtNew\" data-packed=\"false\" data-min-height=\"332\">\r\n<p class=\"font_8\" dir=\"ltr\">         <span class=\"color_18\">PLAY :</span>      Game 1.  plays each Monday at 7:30 PM  and last hand at 9:00PM.  The top six chip holders in the room will move on to the final table, they will be joined by 2 players from the previous week.</p>\r\n<p class=\"font_8\" dir=\"ltr\">        Game 2. Deals in at 9:30PM those guests that are not on the finals table may play on the consolation table(s) which play through 11PM. Then the top 2 chip holders on Consolation tables will be eligible for the following week\'s Final Table!</p>\r\n<p class=\"font_8\" dir=\"ltr\">        We begin each game with new set of chips and do not carry over chips from table to table, nor do we refill tables. Every house has their rules and so do we, and first is do no harm to the game. This means questions about a call, a bet or a house rule are answered by house, and generally not in mid game. Specific issues that have been addressed will be posted below for reference. If it hasn\'t been played here that way before, its not starting tonight! Suggestions for improving the play or action are always appreciated and can be submitted attached to a $20 Bill to the Pit Boss.</p>\r\n\r\n</div>\r\n<div id=\"ie6a3lmt\" class=\"txtNew\" data-packed=\"true\">\r\n<p class=\"font_8\" dir=\"ltr\"><span class=\"color_18\">        WEEKLY PRIZES </span>:</p>\r\n<p class=\"font_8\" dir=\"ltr\">       1st $30*</p>\r\n<p class=\"font_8\" dir=\"ltr\">            2nd $25*</p>\r\n<p class=\"font_8\" dir=\"ltr\">                     3rd $20*</p>\r\n<p class=\"font_8\" dir=\"ltr\">                             4th $15*</p>\r\n<p class=\"font_8\" dir=\"ltr\">       *Top 4 Chip holders on the Finals Table each week will receive gift certifcates that are good at Trinity Hall on the winner\'s next visit.</p>\r\n<p class=\"font_8\" dir=\"ltr\">If you enjoy playing free poker online, and want to play around real people that are nice, or people that are real nice, you may have found your game!</p>\r\n<p class=\"font_8\" dir=\"ltr\">        First hands are dealt at 7:30PM &amp; 9:30PM sharp, to avoid missing the deal and losing your seat we suggest joining us early to sign in and then to stay close to your assigned table 10 Minutes before game starts. If you want to refresh your knowledge on winning hands and how we play Pub Poker please download our <a href=\"http://docs.wixstatic.com/ugd/36fbba_ae2a77f05d9f497ab3e9e84467a0901b.pdf\" target=\"_blank\" rel=\"noopener\" data-type=\"document\"><span class=\"color_3\">Pub Poker Rules!</span></a> <a href=\"http://docs.wixstatic.com/ugd/36fbba_e0c48e8efdf140888b9af626b65935aa.pdf\" target=\"_blank\" rel=\"noopener\" data-type=\"document\">to start. Additional items addressed recently include:</a></p>\r\n<p class=\"font_8\" dir=\"ltr\">             Mis-deal becomes Burn Card everytime, there is no choice for player, when misdeal occurs during dealing of 1st cards all players cards are burned and new cards are dealt, if dealer is on 2nd and subsequent cards when mis deal occurs it is only the players; mis dealt card that is burned.</p>\r\n<p class=\"font_8\" dir=\"ltr\">            Straddle Bets are not permitted, due to the variants in this bet, each of which impacts different players and the betting procedure ether by round or hand, we do not and its low level of use by recreational players, we have chosen to not permit its use.</p>\r\n<p class=\"font_8\" dir=\"ltr\">            Birthday Cakes served during the game night must be offered to all players without concern as to what their chip count is. <span class=\"wixGuard\">​</span></p>\r\n<p class=\"font_8\" dir=\"ltr\">            The Provost\'s Screen will be reserved for showing Dancing With The Stars when the program is in season, except on those weeks in which national security, national championships or players at Table #1 do not object to a different program unanimously .</p>\r\n<p class=\"font_8\" dir=\"ltr\">             Comfort &amp; care for any horses ridden in  the Pub for the game,  is the responsibility of the player/rider and not the Pub Staff and should be resolved before sitting down at the table!</p>\r\n<p class=\"font_8\" dir=\"ltr\">             The Pub may at its own and sole discretion  remove a player from the game at any time and for any reason we may also refuse entry to the game</p>\r\n<p class=\"font_8\" dir=\"ltr\"><span class=\"wixGuard\">​</span></p>\r\n\r\n</div>', 'Pocker Night', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2018-11-01 15:37:40', '2018-11-01 15:37:40', '', 30, 'http://localhost/trinityPubProject/30-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2018-11-01 15:38:06', '2018-11-01 15:38:06', '<div id=\"comp-iv12gj43\" class=\"txtNew\" data-packed=\"true\">\r\n<h4 class=\"font_4\" style=\"text-align: center;\">We are THE place to watch Rugby in DFW!</h4>\r\n</div>\r\n<div id=\"comp-jebph7xk\" class=\"txtNew\" data-packed=\"false\" data-min-height=\"747\">\r\n<p class=\"font_7\">We sometimes miss a fixture coming up and if you know a match we could show, please drop us a line or call the pub! We subscribe to NBC Gold Rugby, TheRugbyChannel, ESPN+, and also host Pay Per View events with Premium Sports.</p>\r\n<p class=\"font_7\" style=\"text-align: center;\"><strong><span class=\"color_18\">RUGBY CHAMPIONSHIP 2018</span></strong></p>\r\n<p class=\"font_7\" style=\"text-align: center;\">New Zealand, Australia, South Africa &amp; Argentina</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">12 match tournament</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">8/18   Aus V NZ            8:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">           S.Afr V Arg       10:00A  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">8/25   NZ  V Aus           8:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">          Arg V S.Afr          2:10P LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">9/8     NZ V Arg            8:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">         Aus V S.Afr        10:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">9/15  NZ  V S.Afr          9:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">         Aus V  Arg         10:50A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">9/29 S.Afr V Aus        10:00A  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">         Arg  V  NZ            5:40P  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">10/6 S.Afr V NZ          10:00A  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">          Arg V Aus            5:40P  LIVE</p>\r\n<p class=\"font_7\"> (Replay indicates match was overnight when we were closed and we will open to show it as a replay, so..... NO SPOILERS!)</p>\r\n\r\n</div>', 'Rugby', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2018-11-01 15:38:06', '2018-11-01 15:38:06', '', 26, 'http://localhost/trinityPubProject/26-revision-v1/', 0, 'revision', '', 0),
(62, 1, '2018-11-01 15:38:58', '2018-11-01 15:38:58', '<div id=\"comp-iv12gj43\" class=\"txtNew\" data-packed=\"true\">\r\n<h4 class=\"font_4\" style=\"text-align: center;\">We are THE place to watch Rugby in DFW!</h4>\r\n</div>\r\n<div id=\"comp-jebph7xk\" class=\"txtNew\" data-packed=\"false\" data-min-height=\"747\">\r\n<p class=\"font_7\">We sometimes miss a fixture coming up and if you know a match we could show, please drop us a line or call the pub! We subscribe to NBC Gold Rugby, TheRugbyChannel, ESPN+, and also host Pay Per View events with Premium Sports.</p>\r\n<!--more-->\r\n<p class=\"font_7\" style=\"text-align: center;\"><strong><span class=\"color_18\">RUGBY CHAMPIONSHIP 2018</span></strong></p>\r\n<p class=\"font_7\" style=\"text-align: center;\">New Zealand, Australia, South Africa &amp; Argentina</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">12 match tournament</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">8/18   Aus V NZ            8:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">           S.Afr V Arg       10:00A  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">8/25   NZ  V Aus           8:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">          Arg V S.Afr          2:10P LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">9/8     NZ V Arg            8:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">         Aus V S.Afr        10:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">9/15  NZ  V S.Afr          9:00A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">         Aus V  Arg         10:50A  (Replay)</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">9/29 S.Afr V Aus        10:00A  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">         Arg  V  NZ            5:40P  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">10/6 S.Afr V NZ          10:00A  LIVE</p>\r\n<p class=\"font_7\" style=\"text-align: center;\">          Arg V Aus            5:40P  LIVE</p>\r\n<p class=\"font_7\"> (Replay indicates match was overnight when we were closed and we will open to show it as a replay, so..... NO SPOILERS!)</p>\r\n\r\n</div>', 'Rugby', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2018-11-01 15:38:58', '2018-11-01 15:38:58', '', 26, 'http://localhost/trinityPubProject/26-revision-v1/', 0, 'revision', '', 0),
(63, 1, '2018-11-01 15:39:33', '2018-11-01 15:39:33', '<div id=\"comp-ik4sh3l8\" class=\"txtNew\" data-packed=\"true\">\r\n<h2 class=\"font_2\" style=\"text-align: center;\"><span class=\"color_18\">TV MATCHES</span></h2>\r\n</div>\r\n<div id=\"comp-iv47dyqz\" class=\"txtNew\" data-packed=\"true\">\r\n<p class=\"font_7\">            EPL, La Liga, Bundesliga, Scottish FA, Internationals, MLS?</p>\r\n<p class=\"font_7\">            We Watch Them All &amp; Invite Our Guests To Enjoy Them With Us!</p>\r\n<p class=\"font_7\">            If You\'re Coming To F &amp; Blind, Get In A Row or Give Some Other Fan A Hard Time.... Choose Another Pub, You\'ll Enjoy It More</p>\r\n<p style=\"font-size: 16px;\">            We\'re Family Friendly where Moms can bring their Boys, &amp; Daughters can bring Dad! So aside from an occasional &amp; involuntary exclamation - which can happen, even to a bishop, once..... -  our guests are fans of the game &amp; treat each other with respect. Fans that prefer not to allow the rest of us to enjoy the match will be asked to leave.</p>\r\n<!--more-->\r\n<p style=\"font-size: 16px; text-align: center;\"><strong>NEW TO BEING A FAN OF FOOTBALL in DALLAS? </strong></p>\r\n<p style=\"font-size: 16px;\">            Watched in a pub that was all Hargy Bargy, Push &amp; Shove, FNBlind?  Come along and watch with us where Moms can bring their sons and Dads their daughters to enjoy the beautiful game among passionate fans who understand the difference between the occasional surprising outburst and a 90-minute rant at the Ref, Team, League &amp; passers-by!  Come Watch With Us &amp; Enjoy The Game!</p>\r\n<p style=\"font-size: 16px;\">            OTHER CLUBS MEET: Man City - British Lion (Frisco)  Arsenal,  Liverpool , Newcastle United- Londoner ( Addison)  West Ham - McSwiggans  (The Colony)  Manchester United - Henderson TapHouse (Dallas) Chelsea FC - BBC - (Dallas), AFC Bournemouth  AKA ...THE CHERRIES! - Trinity Hall Right Here!</p>\r\n<p style=\"text-align: center;\"><strong>Thanks for watching with us!</strong></p>\r\n<p style=\"font-size: 16px;\">            Fixtures are posted up to 2 weeks in advance, and may contain errors or incorrect information, most will be updated and corrected before game day! If you are not sure about something you see or read, please call the Pub for clarification!</p>\r\n<p style=\"font-size: 16px;\">           Bar service on Saturday mornings starts at 7AM  Sunday\'s alcohol service available at 10AM, when served with food or Noon if no food has been purchased! It is illegal to bring alcohol into or remove it from a permitted outlet. <span class=\"wixGuard\">​</span></p>\r\n<p style=\"font-size: 16px;\">We may be here &amp; open before bar permit allows sale of alcohol, please keep in mind we follow state law.</p>\r\n\r\n</div>', 'Football', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2018-11-01 15:39:33', '2018-11-01 15:39:33', '', 28, 'http://localhost/trinityPubProject/28-revision-v1/', 0, 'revision', '', 0),
(65, 1, '2018-11-02 09:01:24', '2018-11-02 09:01:24', '<p class=\"font_5\">Especially for our local and regular guests a thank you program that serves up a new offer around the 17th of every month that can be redeemed before the start of the following month!</p>', 'Offer of the Month!', '', 'publish', 'open', 'open', '', 'offer-of-the-month', '', '', '2018-11-02 11:05:50', '2018-11-02 11:05:50', '', 0, 'http://localhost/trinityPubProject/?p=65', 0, 'post', '', 0),
(66, 1, '2018-11-02 09:01:24', '2018-11-02 09:01:24', '<h5 class=\"font_5\">Especially for our local and regular guests a thank you program that serves up a new offer around the 17th of every month that can be redeemed before the start of the following month!</h5>', 'Offer of the Month!', '', 'inherit', 'closed', 'closed', '', '65-revision-v1', '', '', '2018-11-02 09:01:24', '2018-11-02 09:01:24', '', 65, 'http://localhost/trinityPubProject/65-revision-v1/', 0, 'revision', '', 0),
(67, 1, '2018-11-02 11:05:17', '2018-11-02 11:05:17', '<pre class=\"font_5\">Especially for our local and regular guests a thank you program that serves up a new offer around the 17th of every month that can be redeemed before the start of the following month!</pre>', 'Offer of the Month!', '', 'inherit', 'closed', 'closed', '', '65-revision-v1', '', '', '2018-11-02 11:05:17', '2018-11-02 11:05:17', '', 65, 'http://localhost/trinityPubProject/65-revision-v1/', 0, 'revision', '', 0),
(68, 1, '2018-11-02 11:05:50', '2018-11-02 11:05:50', '<p class=\"font_5\">Especially for our local and regular guests a thank you program that serves up a new offer around the 17th of every month that can be redeemed before the start of the following month!</p>', 'Offer of the Month!', '', 'inherit', 'closed', 'closed', '', '65-revision-v1', '', '', '2018-11-02 11:05:50', '2018-11-02 11:05:50', '', 65, 'http://localhost/trinityPubProject/65-revision-v1/', 0, 'revision', '', 0),
(69, 1, '2018-11-08 11:21:38', '2018-11-08 11:21:38', '<h2 class=\"font_2\" style=\"text-align: center;\">You Say Its Your Birthday?</h2>\r\n<p class=\"font_7\"><span class=\"color_18\">Discount Promotion Available Sundays ONLY</span></p>\r\n<p class=\"font_7\"><span class=\"color_18\">(Valid On the Sunday before  OR after your birthday this year!)</span></p>\r\n<p class=\"font_7\">Come celebrate with a party at the pub, and bring all your friends and family!  Trinity Hall offers a unique Birthday Promotion that can save the party up to $150!<!--more--></p>\r\n\r\n<ul class=\"font_7\">\r\n 	<li>\r\n<p class=\"font_7\">Your Age as a % Discount Off your Party\'s Food Check</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">No limit on the number of guests!</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Discount offered on Sunday Before or After Birthdate</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Alcohol is not discounted</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Party can bring their favorite Birthday Cake we\'ll cut &amp; serve</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Maximum Discount is limited to $15 per Adult ordering Sandwich or Entree, child menu items are ineligible.</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">Discount is applied to a single check however payments may be split between guests.</p>\r\n</li>\r\n 	<li>\r\n<p class=\"font_7\">We DO NOT add any automatic gratuities, and respectfully remind parties to consider the pre-discounted total when deciding on any tip for your servers.</p>\r\n</li>\r\n</ul>', 'Birthday Deal!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-11-08 11:21:38', '2018-11-08 11:21:38', '', 1, 'http://localhost/trinityPubProject/1-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2018-11-09 13:50:10', '2018-11-09 13:50:10', '<div id=\"comp-ioepssgm\" class=\"txtNew\" data-packed=\"true\">\r\n<h5 class=\"font_5\">Awaken Your Inner Irish ~ Its About More Than Just Where You\'re From...</h5>\r\n</div>\r\n<div id=\"comp-ioeqa7ie\" class=\"txtNew\" data-packed=\"true\">\r\n<p class=\"font_7\">    \".... Less about the place you are from, and more about the person you are! Your inner  Irish is about enjoying yourself in the company of friends, putting down the phones and picking up the conversations! Trinity Hall Irish Pub will help you find it with a great beer selection, the best whiskey list in Dallas and a friendly staff that enjoys taking care of you!\"</p>\r\n\r\n</div>', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-11-09 13:50:10', '2018-11-09 13:50:10', '', 0, 'http://localhost/trinityPubProject/?page_id=71', 0, 'page', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(72, 1, '2018-11-09 13:50:10', '2018-11-09 13:50:10', '<div id=\"comp-ioepssgm\" class=\"txtNew\" data-packed=\"true\">\r\n<h5 class=\"font_5\">Awaken Your Inner Irish ~ Its About More Than Just Where You\'re From...</h5>\r\n</div>\r\n<div id=\"comp-ioeqa7ie\" class=\"txtNew\" data-packed=\"true\">\r\n<p class=\"font_7\">    \".... Less about the place you are from, and more about the person you are! Your inner  Irish is about enjoying yourself in the company of friends, putting down the phones and picking up the conversations! Trinity Hall Irish Pub will help you find it with a great beer selection, the best whiskey list in Dallas and a friendly staff that enjoys taking care of you!\"</p>\r\n\r\n</div>', 'Home', '', 'inherit', 'closed', 'closed', '', '71-revision-v1', '', '', '2018-11-09 13:50:10', '2018-11-09 13:50:10', '', 71, 'http://localhost/trinityPubProject/?p=72', 0, 'revision', '', 0),
(73, 1, '2018-11-09 13:50:23', '2018-11-09 13:50:23', '', 'News', '', 'publish', 'closed', 'closed', '', 'news', '', '', '2018-11-09 13:50:23', '2018-11-09 13:50:23', '', 0, 'http://localhost/trinityPubProject/?page_id=73', 0, 'page', '', 0),
(74, 1, '2018-11-09 13:50:23', '2018-11-09 13:50:23', '', 'News', '', 'inherit', 'closed', 'closed', '', '73-revision-v1', '', '', '2018-11-09 13:50:23', '2018-11-09 13:50:23', '', 73, 'http://localhost/trinityPubProject/?p=74', 0, 'revision', '', 0),
(75, 1, '2018-11-09 14:04:05', '2018-11-09 14:04:05', ' ', '', '', 'publish', 'closed', 'closed', '', '75', '', '', '2018-11-09 14:04:05', '2018-11-09 14:04:05', '', 0, 'http://localhost/trinityPubProject/?p=75', 2, 'nav_menu_item', '', 0),
(78, 1, '2018-11-09 17:13:39', '2018-11-09 17:13:39', '<h5>WHAT TO EXPECT AT OUR TASTING EVENTS!</h5>\r\n<p class=\"font_7\">Seating sufficient for attendance expected will be set aside in the pub. Each place setting will have Tasting notes, Nosing Glass and a large Glass of Water! Seating is open and guests seat themselves upon arrival. Closer to speakers or off to the side, it is up to each party, and many of our regulars enjoy sitting with new guests so don\'t e afraid to join an exisiting party!<!--more--></p>\r\n<p class=\"font_7\">Our Tasting Night\'s Program begin with the 1st pour at 7:30PM, as it is open seating we suggest joining us between 7-7:15PM to find a seat or table you\'ll enjoy. Our snack items are similar to tapas in portion &amp; style, designed to accompany the spirit, but not necessarily a full meal, some of our guests will join us early or sit with us after the tasting for a bite. Service includes up to 6 sample pours of 3/4oz of an expression, or equivalent of 3 1/2 drinks served over 2 hours. We present checks at the end of your visit so no pre-payment is required, although servers may ask for your credit card initially to ensure that they can set up your tab and be ready for when all want to leave.</p>', 'Tasting', '', 'publish', 'open', 'open', '', 'tasting', '', '', '2018-11-09 17:47:12', '2018-11-09 17:47:12', '', 0, 'http://localhost/trinityPubProject/?p=78', 0, 'post', '', 0),
(79, 1, '2018-11-09 17:13:39', '2018-11-09 17:13:39', 'WHAT TO EXPECT AT OUR TASTING EVENTS!\r\n<p class=\"font_7\">Seating sufficient for attendance expected will be set aside in the pub. Each place setting will have Tasting notes, Nosing Glass and a large Glass of Water! Seating is open and guests seat themselves upon arrival. Closer to speakers or off to the side, it is up to each party, and many of our regulars enjoy sitting with new guests so don\'t e afraid to join an exisiting party!</p>\r\n<p class=\"font_7\">Our Tasting Night\'s Program begin with the 1st pour at 7:30PM, as it is open seating we suggest joining us between 7-7:15PM to find a seat or table you\'ll enjoy. Our snack items are similar to tapas in portion &amp; style, designed to accompany the spirit, but not necessarily a full meal, some of our guests will join us early or sit with us after the tasting for a bite. Service includes up to 6 sample pours of 3/4oz of an expression, or equivalent of 3 1/2 drinks served over 2 hours. We present checks at the end of your visit so no pre-payment is required, although servers may ask for your credit card initially to ensure that they can set up your tab and be ready for when all want to leave.</p>', 'Tasting', '', 'inherit', 'closed', 'closed', '', '78-revision-v1', '', '', '2018-11-09 17:13:39', '2018-11-09 17:13:39', '', 78, 'http://localhost/trinityPubProject/?p=79', 0, 'revision', '', 0),
(80, 1, '2018-11-09 17:14:28', '2018-11-09 17:14:28', '<p class=\"font_7\">Did you know that Irish Coffee was first served at a wee airport in Co.Limerick in Ireland?  On a bad weather night, a flight returned to the airport to await better weather, the passengers were brought to the terminal\'s restaurant to warm up. The Chef whipped up a treat that he knew would bring a little comfort on that cold rainy night...... <!--more-->The guest on tasting the delicious treat asked Chef what kind of coffee it was, Irish Coffee he said with smile, and so began this great tradition!</p>\r\n<p class=\"font_7\">There are plenty of versions now, adding creme de menthe, smokey whiskies, spray can cream, etc... but we serve the way Chef Sheridan intended, except we use a 14 ounce mug so the enjoyment goes on a lot longer!</p>', 'Irish Coffee', '', 'publish', 'open', 'open', '', 'irish-coffee', '', '', '2018-11-09 18:04:07', '2018-11-09 18:04:07', '', 0, 'http://localhost/trinityPubProject/?p=80', 0, 'post', '', 0),
(81, 1, '2018-11-09 17:14:28', '2018-11-09 17:14:28', '<p class=\"font_7\">Did you know that Irish Coffee was first served at a wee airport in Co.Limerick in Ireland?  On a bad weather night, a flight returned to the airport to await better weather, the passengers were brought to the terminal\'s restaurant to warm up. The Chef whipped up a treat that he knew would bring a little comfort on that cold rainy night...... The guest on tasting the delicious treat asked Chef what kind of coffee it was, Irish Coffee he said with smile, and so began this great tradition!</p>\r\n<p class=\"font_7\">There are plenty of versions now, adding creme de menthe, smokey whiskies, spray can cream, etc... but we serve the way Chef Sheridan intended, except we use a 14 ounce mug so the enjoyment goes on a lot longer!</p>', 'Irish Coffee', '', 'inherit', 'closed', 'closed', '', '80-revision-v1', '', '', '2018-11-09 17:14:28', '2018-11-09 17:14:28', '', 80, 'http://localhost/trinityPubProject/?p=81', 0, 'revision', '', 0),
(82, 1, '2018-11-09 17:45:12', '2018-11-09 17:45:12', '<p class=\"font_7\">Did you know that Irish Coffee was first served at a wee airport in Co.Limerick in Ireland?  On a bad weather night, a flight returned to the airport to await better weather, the passengers were brought to the terminal\'s restaurant to warm up. The Chef whipped up a treat that he knew would bring a little comfort on that cold rainy night...... <!--more-->The guest on tasting the delicious treat asked Chef what kind of coffee it was, Irish Coffee he said with smile, and so began this great tradition!</p>\r\n<p class=\"font_7\">There are plenty of versions now, adding creme de menthe, smokey whiskies, spray can cream, etc... but we serve the way Chef Sheridan intended, except we use a 14 ounce mug so the enjoyment goes on a lot longer!</p>', 'Irish Coffee', '', 'inherit', 'closed', 'closed', '', '80-revision-v1', '', '', '2018-11-09 17:45:12', '2018-11-09 17:45:12', '', 80, 'http://localhost/trinityPubProject/?p=82', 0, 'revision', '', 0),
(83, 1, '2018-11-09 17:45:25', '2018-11-09 17:45:25', 'WHAT TO EXPECT AT OUR TASTING EVENTS!\r\n<p class=\"font_7\">Seating sufficient for attendance expected will be set aside in the pub. Each place setting will have Tasting notes, Nosing Glass and a large Glass of Water! Seating is open and guests seat themselves upon arrival. Closer to speakers or off to the side, it is up to each party, and many of our regulars enjoy sitting with new guests so don\'t e afraid to join an exisiting party!<!--more--></p>\r\n<p class=\"font_7\">Our Tasting Night\'s Program begin with the 1st pour at 7:30PM, as it is open seating we suggest joining us between 7-7:15PM to find a seat or table you\'ll enjoy. Our snack items are similar to tapas in portion &amp; style, designed to accompany the spirit, but not necessarily a full meal, some of our guests will join us early or sit with us after the tasting for a bite. Service includes up to 6 sample pours of 3/4oz of an expression, or equivalent of 3 1/2 drinks served over 2 hours. We present checks at the end of your visit so no pre-payment is required, although servers may ask for your credit card initially to ensure that they can set up your tab and be ready for when all want to leave.</p>', 'Tasting', '', 'inherit', 'closed', 'closed', '', '78-revision-v1', '', '', '2018-11-09 17:45:25', '2018-11-09 17:45:25', '', 78, 'http://localhost/trinityPubProject/?p=83', 0, 'revision', '', 0),
(84, 1, '2018-11-09 17:46:40', '2018-11-09 17:46:40', '<h5 class=\"font_8\" style=\"text-align: left;\">Sundays 7:30pm! What is Pub Quiz Trivia and How Does It Play?</h5>\r\n<p class=\"font_7\">Also known as a \"Table Quiz\"  or \"Trivia Night\", Trinity Hall\'s Sunday Trivia is considered one of the most challenging in town! We kick off to a full house almost every Sunday at 7:30 PM, QuizMaster Roger brings 4 rounds of 12 questions that include a nice mix of Politics, Movies, Geography, Sports, Current Events and General Trivia in each round.<!--more--></p>\r\n<p class=\"font_7\">There\'s also a handout each week with pictures or puzzles to solve during the evening, and a unique progressive Bonus question, offered to one lucky team - answer it correctly and take home the cash, miss it and the pot grows for next week!  Teams as big as your table, lots of fun and family friendly. Gather your best team and come play this Sunday evening, its free to play &amp; prizes are awarded!</p>', 'Pub Quiz Trivia', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-11-09 17:46:40', '2018-11-09 17:46:40', '', 5, 'http://localhost/trinityPubProject/?p=84', 0, 'revision', '', 0),
(85, 1, '2018-11-09 17:47:12', '2018-11-09 17:47:12', '<h5>WHAT TO EXPECT AT OUR TASTING EVENTS!</h5>\r\n<p class=\"font_7\">Seating sufficient for attendance expected will be set aside in the pub. Each place setting will have Tasting notes, Nosing Glass and a large Glass of Water! Seating is open and guests seat themselves upon arrival. Closer to speakers or off to the side, it is up to each party, and many of our regulars enjoy sitting with new guests so don\'t e afraid to join an exisiting party!<!--more--></p>\r\n<p class=\"font_7\">Our Tasting Night\'s Program begin with the 1st pour at 7:30PM, as it is open seating we suggest joining us between 7-7:15PM to find a seat or table you\'ll enjoy. Our snack items are similar to tapas in portion &amp; style, designed to accompany the spirit, but not necessarily a full meal, some of our guests will join us early or sit with us after the tasting for a bite. Service includes up to 6 sample pours of 3/4oz of an expression, or equivalent of 3 1/2 drinks served over 2 hours. We present checks at the end of your visit so no pre-payment is required, although servers may ask for your credit card initially to ensure that they can set up your tab and be ready for when all want to leave.</p>', 'Tasting', '', 'inherit', 'closed', 'closed', '', '78-revision-v1', '', '', '2018-11-09 17:47:12', '2018-11-09 17:47:12', '', 78, 'http://localhost/trinityPubProject/?p=85', 0, 'revision', '', 0),
(86, 1, '2018-11-09 18:03:58', '2018-11-09 18:03:58', '', 'irishCoffee', '', 'inherit', 'open', 'closed', '', 'irishcoffee', '', '', '2018-11-09 18:03:58', '2018-11-09 18:03:58', '', 80, 'http://localhost/trinityPubProject/wp-content/uploads/2018/11/irishCoffee.jpeg', 0, 'attachment', 'image/jpeg', 0),
(95, 1, '2018-11-12 09:19:47', '2018-11-12 09:19:47', '', 'offerOfMonth', '', 'inherit', 'open', 'closed', '', 'offerofmonth', '', '', '2018-11-12 09:20:26', '2018-11-12 09:20:26', '', 0, 'http://localhost/trinityPubProject/wp-content/uploads/2018/11/offerOfMonth.jpg', 0, 'attachment', 'image/jpeg', 0),
(96, 1, '2018-11-12 09:21:07', '2018-11-12 09:21:07', 'http://localhost/trinityPubProject/wp-content/uploads/2018/11/cropped-offerOfMonth.jpg', 'cropped-offerOfMonth.jpg', '', 'inherit', 'open', 'closed', '', 'cropped-offerofmonth-jpg', '', '', '2018-11-12 09:21:07', '2018-11-12 09:21:07', '', 0, 'http://localhost/trinityPubProject/wp-content/uploads/2018/11/cropped-offerOfMonth.jpg', 0, 'attachment', 'image/jpeg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

DROP TABLE IF EXISTS `wp_termmeta`;
CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Primary Menu Links', 'primary-menu-links', 0),
(3, 'Footer Menu Links', 'footer-menu-links', 0),
(4, 'Events', 'events', 0),
(5, 'post-format-aside', 'post-format-aside', 0),
(6, 'We serve', 'we-serve', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 4, 0),
(5, 4, 0),
(16, 2, 0),
(17, 2, 0),
(18, 2, 0),
(19, 3, 0),
(20, 3, 0),
(21, 3, 0),
(22, 3, 0),
(65, 4, 0),
(65, 5, 0),
(75, 2, 0),
(78, 6, 0),
(80, 6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'nav_menu', '', 0, 4),
(3, 3, 'nav_menu', '', 0, 4),
(4, 4, 'category', '', 0, 3),
(5, 5, 'post_format', '', 0, 1),
(6, 6, 'category', '', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', 'Whiskey drinking, trad music playing, soccer &amp; rugby watching, Irish breakfast dining, \r\nfamily-friendly service, Irish Pub &amp; Restaurant at Mockingbird Station in Dallas'),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'locale', ''),
(11, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(12, 1, 'wp_user_level', '10'),
(13, 1, 'dismissed_wp_pointers', 'theme_editor_notice'),
(14, 1, 'show_welcome_panel', '1'),
(15, 1, 'session_tokens', 'a:1:{s:64:\"9a7cf44cc1e1041370fdd57649c0130a39163ea658f3fa23b07af6f2effed3e6\";a:4:{s:10:\"expiration\";i:1548773807;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:104:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36\";s:5:\"login\";i:1547564207;}}'),
(16, 1, 'wp_dashboard_quick_press_last_post_id', '3'),
(17, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:11:\"5.128.255.0\";}'),
(18, 1, 'wp_user-settings', 'mfold=o&libraryContent=browse&hidetb=1&editor=tinymce'),
(19, 1, 'wp_user-settings-time', '1541785508'),
(20, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(21, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:\"add-post_tag\";}'),
(22, 1, 'nav_menu_recently_edited', '2'),
(23, 1, 'closedpostboxes_post', 'a:0:{}'),
(24, 1, 'metaboxhidden_post', 'a:7:{i:0;s:12:\"revisionsdiv\";i:1;s:13:\"trackbacksdiv\";i:2;s:10:\"postcustom\";i:3;s:16:\"commentstatusdiv\";i:4;s:11:\"commentsdiv\";i:5;s:7:\"slugdiv\";i:6;s:9:\"authordiv\";}'),
(25, 1, 'wp_media_library_mode', 'list'),
(26, 1, 'syntax_highlighting', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$B5EXYl3D6hzvUBJ0oAZIXR2UmVKO8V0', 'admin', 'nina_roun@mail.ru', '', '2018-10-11 11:01:38', '', 0, 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=338;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
